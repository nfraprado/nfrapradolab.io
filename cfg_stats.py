#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This configuration is only used when you `make stats`. It loads the stats
# plugin. This is done to avoid the stats plugin to be loaded on normal usage.

import os
import sys

sys.path.append(os.curdir)
from cfg_main import *

PLUGINS += ["stats"]
