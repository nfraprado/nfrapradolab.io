#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This configuration is only used when you `make publish`. Settings only
# relevant when publishing the site go here.

import os
import sys

sys.path.append(os.curdir)
from cfg_main import *

SITEURL = "https://nfraprado.net"

OUTPUT_PATH = "public/"

# Make each i18n subsite feed only show posts in its language
TRANSLATION_FEED_ATOM = "feeds/all.atom.xml"
CATEGORY_FEED_ATOM = "feeds/{slug}.atom.xml"

DELETE_OUTPUT_DIRECTORY = True
