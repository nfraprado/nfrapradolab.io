#!/bin/python
import matplotlib as mpl
import matplotlib.pyplot as plt
import json

LANG = "en"
xlabel = {"en": "Day of month", "br": "Dia do mês"}
ylabel = {"en": "Number of posts published", "br": "Número de artigos publicados"}

stats = {}
with open("stats.json", "r") as f:
    stats = json.loads(f.read())

dom_publish_count = stats["dom_publish_count"]
plt.bar(dom_publish_count.keys(), dom_publish_count.values())
plt.grid(axis="y", linestyle="--")
plt.xlim(left=18)
plt.xlabel(xlabel[LANG])
plt.ylabel(ylabel[LANG])
plt.gcf().set_size_inches(20, 11)
plt.savefig(f"dom-publish-count_{LANG}.png", dpi=150, bbox_inches="tight")
