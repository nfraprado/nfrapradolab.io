#!/bin/python
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import json
from matplotlib.transforms import ScaledTranslation

LANG = "en"
ylabel = {"en": "Word count", "br": "Número de palavras"}

stats = {}
with open("stats.json", "r") as f:
    stats = json.loads(f.read())

dates = []
counts_en = []
counts_br = []
for id, stat in stats["article"].items():
    dates.append(stat["en"]["date"] + "\n" + '"' + id + '"')
    counts_en.append(stat["en"]["word_count"])
    counts_br.append(stat["br"]["word_count"])

bar_width = 0.4
X = np.arange(len(stats["article"]))
plt.bar(X - bar_width / 2, counts_en, bar_width, label="en")
plt.bar(X + bar_width / 2, counts_br, bar_width, label="br")
plt.xticks(X, dates, rotation=45, horizontalalignment="right")

# Offset x labels to fixup alignment
dx, dy = 30, 0
offset = ScaledTranslation(
    dx / plt.gcf().dpi, dy / plt.gcf().dpi, plt.gcf().dpi_scale_trans
)
# apply offset to all xticklabels
for label in plt.gca().get_xmajorticklabels():
    label.set_transform(label.get_transform() + offset)

plt.legend()
plt.grid(axis="y", linestyle="--")
plt.ylabel(ylabel[LANG])
plt.gcf().set_size_inches(20, 11)
plt.savefig(f"word-count_{LANG}.png", dpi=150, bbox_inches="tight")
