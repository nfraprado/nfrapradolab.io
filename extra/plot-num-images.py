#!/bin/python
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import json
from matplotlib.transforms import ScaledTranslation

LANG = "en"
ylabel = {"en": "Number of images", "br": "Número de imagens"}

stats = {}
with open("stats.json", "r") as f:
    stats = json.loads(f.read())

dates = []
num_images = []
for id, stat in stats["article"].items():
    dates.append(stat["en"]["date"] + "\n" + '"' + id + '"')
    num_images.append(stat["en"]["num_imgs"])

X = np.arange(len(stats["article"]))
plt.bar(X, num_images)
plt.xticks(X, dates, rotation=45, horizontalalignment="right")

# Offset x labels to fixup alignment
dx, dy = 30, 0
offset = ScaledTranslation(
    dx / plt.gcf().dpi, dy / plt.gcf().dpi, plt.gcf().dpi_scale_trans
)
# apply offset to all xticklabels
for label in plt.gca().get_xmajorticklabels():
    label.set_transform(label.get_transform() + offset)

plt.grid(axis="y", linestyle="--")
plt.ylabel(ylabel[LANG])
plt.gcf().set_size_inches(20, 11)
plt.savefig(f"num-images_{LANG}.png", dpi=150, bbox_inches="tight")
