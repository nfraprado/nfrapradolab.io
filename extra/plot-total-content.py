#!/bin/python
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import json
from matplotlib.transforms import ScaledTranslation

LANG = "en"
IMAGE_WORD_WORTH = 75
GIF_MULTIPLIER = 3
ylabel = {"en": "Total content estimate", "br": "Estimativa de conteúdo total"}

stats = {}
with open("stats.json", "r") as f:
    stats = json.loads(f.read())

dates = []
total_image_content = []
word_count = []
for id, stat in stats["article"].items():
    dates.append(stat["en"]["date"] + "\n" + '"' + id + '"')

    num_imgs = stat["en"]["num_imgs"]
    num_gifs = stat["en"]["num_gifs"]
    content = (num_imgs - num_gifs) * IMAGE_WORD_WORTH + num_gifs * IMAGE_WORD_WORTH * GIF_MULTIPLIER
    total_image_content.append(content)

    word_count.append(stat[LANG]["word_count"])

X = np.arange(len(stats["article"]))
plt.bar(X, word_count, label="Text")
plt.bar(X, total_image_content, bottom=word_count, label="Images")
plt.xticks(X, dates, rotation=45, horizontalalignment="right")

# Offset x labels to fixup alignment
dx, dy = 30, 0
offset = ScaledTranslation(
    dx / plt.gcf().dpi, dy / plt.gcf().dpi, plt.gcf().dpi_scale_trans
)
# apply offset to all xticklabels
for label in plt.gca().get_xmajorticklabels():
    label.set_transform(label.get_transform() + offset)

plt.grid(axis="y", linestyle="--")
plt.ylabel(ylabel[LANG])
plt.legend()
plt.gcf().set_size_inches(20, 11)
plt.savefig(f"total-content_{LANG}.png", dpi=150, bbox_inches="tight")
