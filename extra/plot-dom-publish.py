#!/bin/python
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import json
from matplotlib.transforms import ScaledTranslation
import datetime

LANG = "en"
ylabel = {"en": "Day of the month", "br": "Dia do mês"}

stats = {}
with open("stats.json", "r") as f:
    stats = json.loads(f.read())

dates = []
days = []
for id, stat in stats["article"].items():
    dates.append(stat["en"]["date"] + "\n" + '"' + id + '"')
    days.append(datetime.datetime.strptime(stat["en"]["date"], "%Y-%m-%d").day)

X = np.arange(len(stats["article"]))
plt.scatter(X, days)
plt.yticks(range(20, 31+1))
plt.xticks(X, dates, rotation=45, horizontalalignment="right")

# Offset x labels to fixup alignment
dx, dy = 30, 0
offset = ScaledTranslation(
    dx / plt.gcf().dpi, dy / plt.gcf().dpi, plt.gcf().dpi_scale_trans
)
# apply offset to all xticklabels
for label in plt.gca().get_xmajorticklabels():
    label.set_transform(label.get_transform() + offset)

plt.grid(axis="y", linestyle="--")
plt.ylabel(ylabel[LANG])
plt.gcf().set_size_inches(20, 11)
plt.savefig(f"dom-publish_{LANG}.png", dpi=150, bbox_inches="tight")
