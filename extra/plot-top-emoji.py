#!/bin/python
import matplotlib as mpl
import matplotlib.pyplot as plt
import json
import matplotlib
import matplotlib.font_manager

# Rendering emojis properly requires this backend
# Remember to apply freetype and matplotlib patches and run mplcairo from git.
matplotlib.use("module://mplcairo.gtk")
matplotlib.font_manager.fontManager.addfont("/usr/share/fonts/noto/NotoColorEmoji.ttf")

LANG = "en"
ylabel = {"en": "Number of occurrences", "br": "Número de ocorrências"}

stats = {}
with open("stats.json", "r") as f:
    stats = json.loads(f.read())

emojis = {}
for id, stat in stats["article"].items():
    for emoji in stat["en"]["emojis"]:
        if emoji in emojis:
            emojis[emoji] += 1
        else:
            emojis[emoji] = 1

emojis = dict(sorted(emojis.items(), key=lambda x: x[1], reverse=True))
plt.bar(emojis.keys(), emojis.values())
plt.grid(axis="y", linestyle="--")
plt.ylabel(ylabel[LANG])
plt.xticks(fontname="noto color emoji", size=20)
plt.gcf().set_size_inches(20, 11)
plt.savefig(f"top-emoji_{LANG}.png", dpi=150, bbox_inches="tight")
