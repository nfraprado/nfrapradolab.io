"""
Based on pelican.readers and sphinx.writers.html5
"""

from pelican import signals
from pelican.readers import RstReader
from docutils.writers.html4css1 import HTMLTranslator, Writer

# For {code} functionality
from io import StringIO
import docutils
import re
import os

# TODO: Find out if it's possible to have two separate readers, one for the NoTT
# functionality and the other for {code}, since having the two mixed like this
# is very confusing. If that's not possible at least give a more generic name to
# this plugin.

class RstNoTtTranslator(HTMLTranslator):

    def visit_literal(self, node):
        self.body.append(self.starttag(node, 'code', '', CLASS='docutils literal'))

    def depart_literal(self, node):
        self.body.append('</code>')

class RstNoTtHTMLWriter(Writer):

    def __init__(self):
        super().__init__()
        self.translator_class = RstNoTtTranslator

class _FieldBodyTranslator(HTMLTranslator):

    def __init__(self, document):
        super().__init__(document)
        self.compact_p = None

    def astext(self):
        return ''.join(self.body)

    def visit_field_body(self, node):
        pass

    def depart_field_body(self, node):
        pass

class RstNoTtReader(RstReader):

    writer_class = RstNoTtHTMLWriter
    field_body_translator_class = _FieldBodyTranslator

    # Custom: match {code} in include directive
    re_code = re.compile(r"(\.\. include:: ){code}")

    def _get_publisher(self, source_path):
        extra_params = {'initial_header_level': '2',
                        'syntax_highlight': 'short',
                        'input_encoding': 'utf-8',
                        'language_code': self._language_code,
                        'halt_level': 2,
                        'traceback': True,
                        'warning_stream': StringIO(),
                        'embed_stylesheet': False}
        user_params = self.settings.get('DOCUTILS_SETTINGS')
        if user_params:
            extra_params.update(user_params)

        pub = docutils.core.Publisher(
            writer=self.writer_class(),
            # Custom: Use string instead of file path as input, so we can edit
            # the contents before it is parsed
            source_class=docutils.io.StringInput,
            destination_class=docutils.io.StringOutput
        )
        pub.set_components('standalone', 'restructuredtext', 'html')
        pub.process_programmatic_settings(None, extra_params, None)

        # Custom: substitute {code} for the code dir for this post
        code_path = self.settings['PATH'] + '/' + self.settings['CODE_DIR']
        with open(source_path) as f:
            source = f.read()

            re_metadata = re.compile(self.settings['FILENAME_METADATA'])
            article_id = re_metadata.match(os.path.basename(source_path)).group('id')

            # Maintain the group 1 since we only want to subsitute '{code}'.
            new_source = self.re_code.sub(r"\1" + str(code_path) + "/" + article_id, source)
            # Passing source_path as well in case we want to use relative paths
            pub.set_source(source_path=source_path, source=new_source)

        pub.publish()
        return pub

def add_reader(readers):
    readers.reader_classes['rst'] = RstNoTtReader

def register():
    signals.readers_init.connect(add_reader)
