from __future__ import absolute_import

import codecs

from pelican import signals
from pelican.generators import Generator

from linker import linker

class ImageLinker(linker.LinkerBase):
    commands = ['image']

    def link(self, link):
        link.path = '/images/' + link.content_object.id + link.path

def register():
    linker.register()
