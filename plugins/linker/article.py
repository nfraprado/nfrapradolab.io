from __future__ import absolute_import

import codecs
import pelican.contents

from pelican import signals
from pelican.generators import Generator

from linker import linker

class ArticleLinker(linker.LinkerBase):
    commands = ['article']
    link_format = {
        "en": '"{}" post',
        "br": 'artigo "{}"'
    }

    def link(self, link):
        for target_article in link.context['content_objects']:
            if isinstance(target_article, pelican.contents.Article):
                lang = link.content_object.lang
                if link.path == target_article.metadata['id'] and target_article.metadata['lang'] == lang:
                    link.text = self.link_format[lang].format(target_article.metadata["title"])
                    link.path = "/"
                    if lang != "en":
                        link.path += lang + "/"
                    link.path += target_article.url
                    return

def register():
    linker.register()
