# -*- coding: utf-8 -*-
"""
Generate statistics about the articles
"""

from pelican import signals
import pelican
import json
import re
import emoji


def generate_article_stats(article):
    stats = {}
    if not isinstance(article, pelican.contents.Article) or article.status == "draft":
        return

    re_img = re.compile(r"<img [^>]*src=\"(?P<src>[^\"]*)\"")
    imgs = list(re_img.finditer(article.content))
    num_gifs = 0
    for i in imgs:
        if i.group('src').endswith('gif'):
            num_gifs += 1
    stats["num_imgs"] = len(imgs)
    stats["num_gifs"] = num_gifs

    re_link = re.compile(r"<a ")
    stats["num_links"] = len(re_link.findall(article.content))

    emojis = [c for c in article.content if c in emoji.UNICODE_EMOJI["en"]]
    stats["emojis"] = emojis

    re_article_link = re.compile(r"<{article}[-\w_]*>")
    with open(article.source_path) as f:
        source = f.read()
        stats["num_article_links"] = len(re_article_link.findall(source))

    # The article.content is the generated output for the article, which
    # contains lots of HTML markup, primarily in the include blocks. Filter
    # these tags out so that the word count is closer to the true: text + code
    word_count_text = article.content
    re_remove = re.compile(
        r"</?(pre|span|code|div|ul|a|table|col|tbody|th|td|tr|img)[^<>]*>"
    )
    word_count_text = re_remove.sub("", article.content)
    stats["word_count"] = len(word_count_text.split())

    stats["date"] = article.date.strftime("%Y-%m-%d")

    article.stats = stats


def generate_global_stats(app):
    stats = {
        "article": {},
        "lang_word_total": {"en": 0, "br": 0},
        "dom_publish_count": {},
    }

    for i in range(32):
        stats["dom_publish_count"][i] = 0

    # Provided by 'content_objects' (linker) plugin
    for content in app.settings["content_objects"]:
        if isinstance(content, pelican.contents.Article):
            article = content

            if article.status == "draft":
                continue

            if article.id not in stats["article"]:
                stats["article"][article.id] = {}
            stats["article"][article.id][article.lang] = article.stats

            if article.lang == "en":  # Avoid double-counting
                stats["dom_publish_count"][article.date.day] += 1

            stats["lang_word_total"][article.lang] += article.stats["word_count"]

    # Sort en/br variants and article keys so they are reproducible and can
    # easily be compared
    for k, v in stats["article"].items():
        new_dict = {}
        new_dict["en"] = v["en"]
        new_dict["br"] = v["br"]
        stats["article"][k] = new_dict
    # articles sorted by date. Assumes en and br dates are always equal.
    stats["article"] = dict(sorted(stats["article"].items(), key=lambda x:
                                   x[1]["en"]["date"]))

    save_stats(stats, "stats.json")


def save_stats(stats, filename):
    stats_json = json.dumps(stats)
    with open(filename, "w") as f:
        f.write(stats_json)


def register():
    signals.content_object_init.connect(generate_article_stats)
    signals.finalized.connect(generate_global_stats)
