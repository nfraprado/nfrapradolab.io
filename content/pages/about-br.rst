#####
Sobre
#####

:icon: info-circle

.. image:: {static}/images/profile.png
   :width: 25%
   :align: right

Meu nome é Nícolas F. R. A. Prado, sou brasileiro e formado em engenharia
elétrica.

Os tópicos em que tenho mais interesse são os relacionados a eletrônica e
software e hardware de computadores.

Além disso, eu me identifico com as culturas hacker e de software livre.

Atualmente estou focando em sistemas embarcados e Linux.

*"Explorar, aprender, compartilhar."*

Links
=====

Gitlab: https://gitlab.com/nfraprado/

Github: https://github.com/nfraprado/

Contato
=======

Fique à vontade para entrar em contato comigo através de `blog@nfraprado.net
<mailto:blog@nfraprado.net>`_.
