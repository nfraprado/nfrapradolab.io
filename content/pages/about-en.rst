#####
About
#####

:icon: info-circle

.. image:: {static}/images/profile.png
   :width: 25%
   :align: right

My name is Nícolas F. R. A. Prado and I'm an Electrical Engineer from Brazil.

I'm really interested in topics related to electronics and computer software and
hardware.

I also identify with the free software and hacker cultures.

Currently focusing on embedded systems and Linux.

*"Explore, learn, share."*

Links
=====

Gitlab: https://gitlab.com/nfraprado/

Github: https://github.com/nfraprado/

Contact
=======

Feel free to reach me at `blog@nfraprado.net <mailto:blog@nfraprado.net>`_.
