from mpd import MPDClient

client = MPDClient()
client.timeout = 60
client.idletimeout = None
client.connect("localhost", 6600)

class MPDPlaylist():

    def __init__(self, query, name=''):
        self.name = name
        if isinstance(query, self.__class__):
            self.songs = set(query.songs)
        elif isinstance(query, set):
            self.songs = set(query)
        else:
            self.songs = self.query_songs(query)


    def __repr__(self):
        return f"{self.__class__}({self.name}, {self.songs})"


    def __or__(self, other):
        return MPDPlaylist(self.songs.union(other.songs))


    def __and__(self, other):
        return MPDPlaylist(self.songs.intersection(other.songs))


    def __neg__(self):
        return MPDPlaylist(MPDPlaylist({}).songs.difference(self.songs))


    def query_songs(self, queries):
        if queries:
            args = []
            for k,v in queries.items():
                args.append(k)
                args.append(v)
            return set(song.get('file') for song in client.search(*args))
        else:
            return set(song.get('file') for song in client.listall() if
                       song.get('file'))


    def write_to_file(self, base):
        if self.name:
            with open(f"{base}/{self.name}.m3u", 'w') as f:
                f.write('\n'.join(self.songs) + '\n')
        else:
            print("Can't write playlist with no name!")
