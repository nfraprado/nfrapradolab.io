from mpd_playlist import MPDPlaylist as PL

saved  = PL({'base': "genres"}, "0_Saved")
buffer = PL({'base': "buffer"}, "1_Buffer")
fvt    = PL({'file': "/%"}, "1_FvtAlbums")

br           = PL({'base': "genres/br"}           , "2_Br")
classical    = PL({'base': "genres/classical"}    , "2_Classical")
electronic   = PL({'base': "genres/electronic"}   , "2_Electronic")
etc          = PL({'base': "genres/etc"}          , "2_ETC")
instrumental = PL({'base': "genres/instrumental"} , "2_Instrumental")
jazz         = PL({'base': "genres/jazz"}         , "2_Jazz")
pop          = PL({'base': "genres/pop"}          , "2_Pop")
post_rock    = PL({'base': "genres/post-rock"}    , "2_Post-rock")
rap          = PL({'base': "genres/rap"}          , "2_Rap")
rock         = PL({'base': "genres/rock"}         , "2_Rock")
soundtrack   = PL({'base': "genres/soundtrack"}   , "2_Sountrack")

common         = PL(rock | electronic | pop | post_rock | rap, "1_Common")
not_soundtrack = PL(-soundtrack, "2_NotSoundtrack")

tdg       = PL({'artist': "Three Days Grace"}, "3_TDG")
minecraft = PL({'base': "genres/soundtrack/games/%minecraft"}, "3_Minecraft")
lotr      = PL({'base': "genres/soundtrack/movies/lotr"}, "3_LOTR")

background = PL(classical
                | jazz
                | instrumental
                | PL({'artist': "Balmorhea"})
                | PL({'artist': "Tycho"})
                | PL({'artist': "MASTER BOOT RECORD"})
                | PL({'artist': "The Album Leaf"})
                | PL({'base': "genres/soundtrack/animes/tatami_galaxy/%ost"})
                | PL({'base': "genres/soundtrack/games"})
                & -PL({'file': "genres/soundtrack/games/Portal - Still Alive.mp3"})
                & -PL({'file': "genres/soundtrack/games/portal_2/3-13_want_you_gone.mp3"})
                & -PL({'title': "Here Comes Vi"}),
                "1_Background")
