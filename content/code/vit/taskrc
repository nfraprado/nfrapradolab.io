data.location=~/.task/data
dateformat=Y-M-D
search.case.sensitive=no

###
# Colors
###
include /usr/share/doc/task/rc/dark-16.theme

rule.precedence.color=deleted,completed,active,keyword.,tag.,project.,overdue,scheduled,due.today,due,blocked,blocking,recurring,tagged,uda.

color.tag.next=
color.blocking=bold
color.blocked=color8 on black
color.scheduled=black on green

###
# Reports
###
report.next.filter = status:pending type:next limit:page +UNBLOCKED +READY
report.next.columns = id,start.age,priority,project,tags,recur,scheduled.countdown,due.relative,until.remaining,description.count,urgency
report.next.labels = ID,Active,P,Project,Tag,Recur,S,Due,Until,Description,Urg
report.next.sort = urgency-

report.all.columns = id,status.short,uuid.short,start.active,entry.age,end.age,type,depends.indicator,priority,project,tags.count,recur.indicator,wait.remaining,scheduled.remaining,due,until.remaining,description
report.all.labels = ID,St,UUID,A,Age,Done,Type,D,P,Project,Tags,R,Wait,Sch,Due,Until,Description

report.all_valid.columns = id,status.short,uuid.short,start.active,entry.age,end.age,type,depends.indicator,priority,project,tags.count,recur.indicator,wait.remaining,scheduled.remaining,due,until.remaining,description
report.all_valid.labels = ID,St,UUID,A,Age,Done,Type,D,P,Project,Tags,R,Wait,Sch,Due,Until,Description
report.all_valid.filter = (status:pending or status:waiting)

report.in.columns = id,description
report.in.description = Inbox
report.in.filter = status:pending limit:page (type:in)
report.in.labels = ID,Description

report.someday.columns = id,description.count
report.someday.description = Someday/Maybe
report.someday.filter = limit: type:someday status:pending
report.someday.labels = ID,Description

report.standby.columns = id,priority,project,due.relative,description.count,urgency
report.standby.description = WaitingFor
report.standby.labels = ID,P,Project,Due,Description,Urgency
report.standby.filter = limit: type:standby status:pending +READY
report.standby.sort = urgency-

report.objectives.columns = id,priority,project,description.count,urgency
report.objectives.description = Projects
report.objectives.labels = ID,P,Project,Description,Urgency
report.objectives.filter = limit: type:objective status:pending +UNBLOCKED
report.objectives.sort = urgency-

report.type.columns = id,description,type
report.type.description = Type
report.type.filter = status:pending limit:page
report.type.labels = ID,Description,Type

report.cal.columns = id,entry.age,recur.indicator,scheduled,due,description
report.cal.description = Calendar
report.cal.filter = type:cal status:pending limit:page
report.cal.labels = ID,Age,R,Scheduled,Due,Description
report.cal.sort = scheduled

###
# Contexts
###
context.uni=(+@uni or -@rep -@sp -@rua -@trab)
context.rep=(+@rep or -@sp -@uni -@rua -@trab)
context.sp=(+@sp or -@rep -@uni -@rua -@trab)
context.rua=(+@rua)
context.trab=(+@trab)

###
# Coefficients
###
urgency.user.project.grad.coefficient=1.0
urgency.user.tag.prova.coefficient=1.5
urgency.user.tag.@rep.coefficient=2.0
urgency.user.tag.@sp.coefficient=2.0
urgency.user.tag.@uni.coefficient=2.0
urgency.blocking.coefficient=6.0
urgency.active.coefficient=10.0
urgency.scheduled.coefficient=6.0

###
# UDAs
###
uda.type.type = string
uda.type.label = Type
uda.type.values = in,next,objective,someday,standby,cal

uda.priority.values = H,L,
urgency.uda.priority.H.coefficient = 6.0
urgency.uda.priority.L.coefficient = -6.0

uda.difficulty.type = string
uda.difficulty.label = Difficulty
uda.difficulty.values = H,L,
