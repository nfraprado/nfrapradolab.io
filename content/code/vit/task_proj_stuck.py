from tasklib import TaskWarrior

tw = TaskWarrior()

def get_stuck_projects():
    """ Get taskwarrior projects that don't have any next actions assigned to
    them.  Next actions here mean actions of type 'next', 'standby' or 'cal',
    either pending or waiting. """

    projects = tw.tasks.pending().filter('+READY', type='objective')

    next_tasks = tw.tasks.filter('(status:pending or status:waiting) and type:next')
    standby_tasks = tw.tasks.filter('(status:pending or status:waiting) and type:standby')
    cal_tasks = tw.tasks.filter('(status:pending or status:waiting) and type:cal')

    for project in projects:
        count_next = len(next_tasks.filter(project=project['project']))
        count_standby = len(standby_tasks.filter(project=project['project']))
        count_cal = len(cal_tasks.filter(project=project['project']))
        if count_next + count_standby + count_cal == 0:
            yield project


def get_stuck_proj_ids():
    return (str(project['id']) for project in get_stuck_projects())
