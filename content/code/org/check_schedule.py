#!/bin/python

import schedule
import colored

weekday_name = ["2a", "3a", "4a", "5a", "6a", "Sáb", "Dom"]
color = True

print("      ", end='')
for weekday in range(0, 7):
    print(f"{weekday_name[weekday]:15}", end='')
print()


def get_color(text):
    hex_num = hex(hash(text) % (16 ** 6))
    hex_num6 = hex_num[:2] + hex_num[2:].rjust(6, '0')
    return hex_num6.replace('0x', '#')


for hour in range(8, 25):
    for minute in range(0, schedule.max_minute + 1, schedule.gran):
        if hour == 24 and minute != 0:
            break
        print(f"{schedule.format_time(hour, minute):5}" + " ", end='')
        for weekday in range(0, 7):
            text = schedule.get_sched(weekday, hour, minute)
            if color:
                print(colored.stylize(f"{text:15}", colored.fg(get_color(text))),
                      end='')
            else:
                print(f"{text:15}", end='')
        print()
