import datetime

import cur_sched


scheds = [cur_sched.segunda, cur_sched.terça, cur_sched.quarta,
          cur_sched.quinta, cur_sched.sexta, cur_sched.sábado,
          cur_sched.domingo]

gran = 30
max_minute = 60 - gran


def get_cur_wday_time():
    weekday = datetime.datetime.today().weekday()
    hour = datetime.datetime.today().hour
    minute = (datetime.datetime.today().minute // gran) * gran
    if hour == 0:
        hour = 24

    return weekday, hour, minute


def format_time(hour, minute):
    if minute > 0:
        time = f"{hour:02}h{minute:02}"
    else:
        time = f"{hour:02}"

    return time


def get_current():
    weekday, hour, minute = get_cur_wday_time()
    return get_sched(weekday, hour, minute)


def get_sched(weekday, hour, minute):
    for m in range(minute, -1, -gran):
        try:
            return scheds[weekday][format_time(hour, m)]
        except:
            pass
    for h in range(hour - 1, 1, -1):
        for m in range(max_minute, -1, -gran):
            try:
                return scheds[weekday][format_time(h, m)]
            except:
                pass
    return ''


def get_new():
    weekday, hour, minute = get_cur_wday_time()
    try:
        return scheds[weekday][format_time(hour, minute)]
    except:
        return ''
