#######################################
Configurando o mbsync para usar XOAUTH2
#######################################

:date: 2020-07-30
:tags: xoauth2, mbsync, email


Por um bom tempo eu usei o offlineimap_ para sincronizar meus emails entre os
provedores e o meu computador. Ter acesso a todos os meus emails sem precisar de
internet é bem útil. Mas depois de ver a `vantagem gigantesca do mbsync em
relação ao offlineimap em termos de performance`_ e de ter tido problemas de
latência com o offlineimap, eu decidi testar o mbsync_ (o projeto se chama
isync, mas o nome do programa que sincroniza os emails é mbsync).

O principal problema que eu via em usar o mbsync é que dos três emails que eu
uso atualmente, um deles precisa de autenticação via XOAUTH2. Esse mecanismo é
suportado nativamente no offlineimap, mas não no mbsync.

Instalação
==========

O primeiro passo obviamente foi instalar o isync/mbsync.

Em seguida eu instalei uma extensão SASL para o mecanismo XOAUTH2, que no
Arch Linux está disponível no AUR como cyrus-sasl-xoauth2-git_.

Para que o mbsync consiga autenticar usando XOAUTH2, é necessário um programa
que utilize as credenciais da conta para obter o *token* atual. Para isso eu
instalei um pacote python chamado oauth2token, que pode ser instalado do pip_,
ou diretamente do AUR_. Obrigado ao VannTen_ por criar esse pacote e também por
me ajudar a resolver alguns problemas em configurar o mbsync para usar XOAUTH2!

**Obs**: Quando eu instalei o mbsync originalmente, havia um problema_ em usá-lo
com XOAUTH2. Mas no momento de publicação desse artigo isso já foi consertado na
versão 1.3.2.

Configuração
============

Para configurar o oauth2token eu segui as instruções no README_, que consistem
em basicamente criar dois arquivos json com as informações da conta
(``client_id`` e ``client_secret`` devem ser configurados no provedor, no meu
caso o Gmail) em um diretório específico, executar ``oauth2create <provedor>
<conta>`` e logar na conta de email pelo navegador.

Depois de ter configurado o oauth2token, configurar o mbsync pra usá-lo foi bem
simples. Eu apenas adicionei ``PassCmd "oauth2get <provedor> <conta>"`` na seção
``IMAPAccount`` do meu ``.mbsyncrc``, trocando ``<provedor>`` e ``<conta>``
pelos valores que eu utilizei no ``oauth2get``, claro. No meu caso não foi
necessário especificar o mecanismo de autenticação nessa mesma seção usando
``AuthMechs XOAUTH2``, já que XOAUTH2 é considerado o mecanismo mais seguro
instalado, e portanto é o padrão. O que eu precisei fazer foi adicionar
``AuthMechs PLAIN`` na seção ``IMAPAccount`` das outras contas que eu não queria
que utilizassem o XOAUTH2.

Por fim, executei o mbsync para a conta em que eu configurei o XOAUTH2 ("dac").
A saída verbosa (``-V``) mostra que ele autenticou usando XOAUTH2 e tudo
funcionou como esperado::

    [nfraprado@ArchWay ~]$ mbsync -V dac
    Reading configuration file /home/nfraprado/.mbsyncrc
    C: 0/1  B: 0/0  M: +0/0 *0/0 #0/0  S: +0/0 *0/0 #0/0
    Channel dac
    Opening master store dac-remote...
    Resolving imap.gmail.com... ok
    Connecting to imap.gmail.com ([2800:3f0:4003:c02::6c]:993)...
    Opening slave store dac-local...
    Connection is now encrypted
    Logging in...
    Authenticating with SASL mechanism XOAUTH2...
    C: 0/1  B: 0/5  M: +0/0 *0/0 #0/0  S: +0/0 *0/0 #0/0
    Opening master box INBOX...
    Opening slave box INBOX...
    Maildir notice: no UIDVALIDITY, creating new.
    Loading master...
    master: 0 messages, 0 recent
    Loading slave...
    slave: 0 messages, 0 recent
    Synchronizing...
    C: 0/1  B: 1/5  M: +0/0 *0/0 #0/0  S: +0/0 *0/0 #0/0

    [ ... ]

    Opening master box _Sent...
    Opening slave box _Sent...
    Creating slave _Sent...
    Maildir notice: no UIDVALIDITY, creating new.
    Loading master...
    Loading slave...
    slave: 0 messages, 0 recent
    master: 92 messages, 0 recent
    Synchronizing...
    C: 1/1  B: 5/5  M: +0/0 *0/0 #0/0  S: +367/367 *0/0 #0/0

.. _offlineimap: https://www.offlineimap.org/
.. _`vantagem gigantesca do mbsync em relação ao offlineimap em termos de performance`: https://people.kernel.org/mcgrof/replacing-offlineimap-with-mbsync
.. _mbsync: https://sourceforge.net/projects/isync/
.. _cyrus-sasl-xoauth2-git: https://aur.archlinux.org/packages/cyrus-sasl-xoauth2-git/
.. _pip: https://pypi.org/project/oauth2token/
.. _AUR: https://aur.archlinux.org/packages/oauth2token/
.. _VannTen: https://github.com/VannTen
.. _problema: https://sourceforge.net/p/isync/bugs/55/
.. _README: https://github.com/VannTen/oauth2token/blob/master/README.rst
