######################################
Setting up mbsync to work with XOAUTH2
######################################

:date: 2020-07-30
:tags: xoauth2, mbsync, email


For a long time I used offlineimap_ to synchronize my emails between the email
providers and my computer. Having access to all my emails offline on my computer
is pretty handy. But after seeing the `brutal efficiency advantage of mbsync
over offlineimap`_, and having had delay issues with offlineimap myself, I
decided to try mbsync_ out (the project is actually called isync, but the
synchronization program is called mbsync).

The main issue I saw with using mbsync is that of the three different emails
that I currently use, one of them had to use XOAUTH2 for authentication. This is
natively supported by offlineimap but not by mbsync.

Installation
============

The first step of course was to install isync/mbsync.

I then installed a SASL plugin for the XOAUTH2 mechanism, which in Arch Linux is
packaged in AUR as cyrus-sasl-xoauth2-git_.

For mbsync to authenticate through XOAUTH2, a program is needed to use the
account credentials to obtain the current token. For this I installed a python
package called oauth2token which can be installed with pip_, or directly from
AUR_. Thanks to VannTen_ for creating this package and also for helping me solve
some issues with setting up mbsync with XOAUTH2!

**Note**: When I originally installed mbsync, there was an issue_ with using it
with XOAUTH2. At the time of this writing though it was already fixed in version
1.3.2.

Configuration
=============

To configure oauth2token I followed the instructions in the README_, which
consist of basically creating two json files with the account information
(``client_id`` and ``client_secret`` should be setup with the provider, in my
case Gmail) in a specific directory, executing ``oauth2create <provider>
<account>`` and logging into the email account on the browser.

Having set up oauth2token, configuring mbsync to use it was pretty simple. I
just added ``PassCmd "oauth2get <provider> <account>"`` in the ``IMAPAccount``
section of my ``.mbsyncrc``, obviously changing ``<provider>`` and ``<account>``
for the values I configured in oauth2token. In my case it wasn't necessary to
specify the authentication mechanism in that same section with ``AuthMechs
XOAUTH2``, since XOAUTH2 is considered the safest mechanism installed, so it is
the default. Rather I needed to use ``AuthMechs PLAIN`` in the ``IMAPAccount``
section of the other accounts that I didn't want to use XOAUTH2 with.

Finally, I ran mbsync for the account that I configured XOAUTH2 with ("dac").
By making the output verbose (``-V``), it can be seen that it authenticated
using XOAUTH2 and everything worked as expected::

    [nfraprado@ArchWay ~]$ mbsync -V dac
    Reading configuration file /home/nfraprado/.mbsyncrc
    C: 0/1  B: 0/0  M: +0/0 *0/0 #0/0  S: +0/0 *0/0 #0/0
    Channel dac
    Opening master store dac-remote...
    Resolving imap.gmail.com... ok
    Connecting to imap.gmail.com ([2800:3f0:4003:c02::6c]:993)...
    Opening slave store dac-local...
    Connection is now encrypted
    Logging in...
    Authenticating with SASL mechanism XOAUTH2...
    C: 0/1  B: 0/5  M: +0/0 *0/0 #0/0  S: +0/0 *0/0 #0/0
    Opening master box INBOX...
    Opening slave box INBOX...
    Maildir notice: no UIDVALIDITY, creating new.
    Loading master...
    master: 0 messages, 0 recent
    Loading slave...
    slave: 0 messages, 0 recent
    Synchronizing...
    C: 0/1  B: 1/5  M: +0/0 *0/0 #0/0  S: +0/0 *0/0 #0/0

    [ ... ]

    Opening master box _Sent...
    Opening slave box _Sent...
    Creating slave _Sent...
    Maildir notice: no UIDVALIDITY, creating new.
    Loading master...
    Loading slave...
    slave: 0 messages, 0 recent
    master: 92 messages, 0 recent
    Synchronizing...
    C: 1/1  B: 5/5  M: +0/0 *0/0 #0/0  S: +367/367 *0/0 #0/0

.. _offlineimap: https://www.offlineimap.org/
.. _`brutal efficiency advantage of mbsync over offlineimap`: https://people.kernel.org/mcgrof/replacing-offlineimap-with-mbsync
.. _mbsync: https://sourceforge.net/projects/isync/
.. _cyrus-sasl-xoauth2-git: https://aur.archlinux.org/packages/cyrus-sasl-xoauth2-git/
.. _pip: https://pypi.org/project/oauth2token/
.. _AUR: https://aur.archlinux.org/packages/oauth2token/
.. _VannTen: https://github.com/VannTen
.. _issue: https://sourceforge.net/p/isync/bugs/55/
.. _README: https://github.com/VannTen/oauth2token/blob/master/README.rst
