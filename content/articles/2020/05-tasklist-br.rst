###################################################
Criando listas de filmes e jogos usando Taskwarrior
###################################################

:date: 2020-05-25
:tags: cli, taskwarrior, organização


Eu gosto de assistir filmes e jogar jogos, mas também gosto de registrar quais
filmes já assisti e quais jogos já zerei. Fora isso, acabei desenvolvendo o
hábito de dar uma nota para cada filme que assisto.

Alguns anos atrás eu comecei a tentar me manter mais focado e organizado usando
um gerenciador de lista de afazeres *CLI* (com interface de linha de comando)
chamado Taskwarrior_. De repente caiu a ficha que eu também poderia usar essa
ferramenta sensacional para registrar essas listas de filmes e jogos!

Configuração do Taskwarrior
===========================

Se você já conhece o Taskwarrior, então você já sabe que tudo que ele precisa é
de um arquivo de configuração (normalmente um ``.taskrc`` na sua pasta home), e
uma pasta para manter seus dados.

Para manter essas listas separadas das minhas tarefas do Taskwarrior, eu decidi
criar uma pasta (``/home/nfraprado/txt/todo``) separada, com um arquivo de
configuração (``config``) e uma pasta para os dados (``data``).

Dentro do ``config``, primeiro eu defini a pasta de dados:

.. code-block:: ini

    data.location = /home/nfraprado/txt/todo/data

Então removi a cor de tarefas rotuladas e tornei a busca independente de
maiúsculas e minúsculas:

.. code-block:: ini

    color.tagged = none

    search.case.sensitive = no

Eu também criei um *UDA*, atributo definido pelo usuário, chamado Score, para
que eu pudesse adicionar uma nota para um filme após assisti-lo:

.. code-block:: ini

    uda.score.type = numeric
    uda.score.label = Score

Finalmente, adicionei os relatórios (*reports*) de filmes e de jogos. Cada
relatório pode ter um filtro configurado, determinando quais tarefas serão
exibidas nele, e também suas colunas, determinando quais atributos das tarefas
serão mostrados, além de outras configurações.

Para os jogos, eu queria dois relatórios: um chamado ``games.todo``, mostrando
os jogos que eu ainda pretendo zerar, e outro chamado ``games.done`` com os
jogos que já zerei:

.. code-block:: ini

    # Games reports
    report.games.todo.description = Games to complete
    report.games.todo.labels = ID,Title
    report.games.todo.columns = id,description
    report.games.todo.filter = status:pending limit:page project:games

    report.games.done.description = Games completed
    report.games.done.labels = Title,Completed on
    report.games.done.columns = description,end
    report.games.done.filter = status:completed limit:page project:games
    report.games.done.sort = end-

Já para os filmes, eu também adicionei os relatórios de ``todo`` e ``done``, com
a diferença de que o ``done`` também possui uma coluna com o ``score`` (*UDA*
criado anteriormente) para que eu possa ver qual nota dei para cada filme que já
assisti. Além disso, também adicionei um relatório ``movies.rank`` que é
ordenado com base na nota, assim posso ter uma lista dos meus filmes favoritos
🙂:

.. code-block:: ini

    # Movies reports
    report.movies.todo.description = Movies to watch
    report.movies.todo.labels = ID,Tags,Title
    report.movies.todo.columns = id,tags,description
    report.movies.todo.filter = status:pending limit:page project:movies

    report.movies.done.description = Movies seen
    report.movies.done.labels = Title,Tags,Score,Watched on
    report.movies.done.columns = description,tags,score,end
    report.movies.done.filter = status:completed limit:page project:movies
    report.movies.done.sort = end-

    report.movies.rank.description = Movies ranking
    report.movies.rank.labels = Title,Tags,Score
    report.movies.rank.columns = description,tags,score
    report.movies.rank.filter = status:completed limit:page project:movies
    report.movies.rank.sort = score-

Configuração do Bash
====================

Com o Taskwarrior configurado, só o que faltava eram alguns atalhos no meu
``.bashrc`` para que fosse mais conveniente adicionar, completar e ver os filmes
e jogos.

Dentro do meu ``.bashrc``, primeiro adicionei uma variável apontando para o
arquivo de configuração:

.. code-block:: bash

    TODOCONFIG="$HOME/txt/todo/config"

Então adicionei os atalhos para adicionar, listar e listar completos tanto para
os jogos quanto para os filmes. Também adicionei um para marcar um jogo como
completo e um para ver o ranking de filmes. Em todos esses comandos, usei
``rc:$TODOCONFIG`` para que o Taskwarrior use o arquivo de configuração que
adicionei antes. Para os atalhos de adicionar e marcar como completo, apenas
utilizei os comandos ``add`` e ``done`` do Taskwarrior, respectivamente. A
listagem é feita simplesmente utilizando os relatórios anteriormente definidos:

.. code-block:: bash

    # Games list
    alias gamadd='task rc:$TODOCONFIG add project:games'
    alias gamlist='task rc:$TODOCONFIG games.todo'
    alias gamlistdone='task rc:$TODOCONFIG games.done'
    alias gamdone='task rc:$TODOCONFIG done'

    # Movies list
    alias movadd='task rc:$TODOCONFIG add project:movies'
    alias movlist='task rc:$TODOCONFIG movies.todo'
    alias movlistdone='task rc:$TODOCONFIG movies.done'
    alias movrank='task rc:$TODOCONFIG movies.rank'

O atalho para completar um filme não pode ser feito diretamente, já que eu
também quero poder dar uma nota ao completá-lo. Por isso criei uma simples
função em Bash que recebe o ID da tarefa e a nota, aplica a nota dada na tarefa
e a marca como completa:

.. code-block:: bash

    movdone()
    {
        if [ -z "$1" ] || [ -z "$2" ];then
            echo "Usage: movdone id score"
            return 1
        fi
        task rc:$TODOCONFIG modify "$1" score:"$2"
        task rc:$TODOCONFIG done "$1"
    }

Resultado
=========

Com toda essa configuração feita, eu finalmente podia registrar meus filmes e
jogos facilmente.

Adicionar um novo filme para assistir é tão simples quanto um ``movadd
nome_do_filme`` (espaços no nome do filme podem ser usados sem problema).

E depois de assistir um filme, posso marcá-lo como finalizado e dar uma nota com
um ``movdone id nota``. O ``id`` de um filme pode ser obtido usando ``movlist``.

Esses são os filmes que eu planejo assistir::

    [nfraprado@ArchWay ~]$ movlist
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    ID Title
    23 Once Upon a Time In Hollywood
    27 Ready player one
    28 In The Shadow Of The Moon 2007

Os últimos 5 filmes que assisti::

    [nfraprado@ArchWay ~]$ movlistdone
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    Title                                             Tags   Score Watched on
    Arrival                                                      8 2020-05-25
    The Green Mile                                               8 2020-05-17
    The Boy Who Harnessed the Wind                               8 2020-04-25
    Jojo Rabbit                                                  6 2020-04-18
    Back to the Future                                           8 2020-04-12

Os filmes que dei a maior nota (desde que criei essa lista)::

    [nfraprado@ArchWay ~]$ movrank
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    Title                                             Tags   Score
    The Lord of the Rings: The Return of the King               10
    The Lord of the Rings: The Two Towers                       10
    The Lord of the Rings: The Fellowship of the Ring           10
    Whisper of the Heart                              ghibli     9
    Apollo 11                                         doc        9
    Life is beautiful                                            9
    Koe no katachi                                               9

Alguns jogos que pretendo zerar::

    [nfraprado@ArchWay ~]$ gamlist
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    ID Title
     6 FTL
     7 The Witness
     8 Factorio
     9 Stardew Valley
    10 Half Life

E os últimos 5 jogos que eu zerei::

    [nfraprado@ArchWay ~]$ gamlistdone
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    Title                                                      Completed on
    The Stanley Parable                                        2020-04-24
    Undertale                                                  2020-04-17
    Cave Story                                                 2020-03-30
    Antichamber                                                2020-03-22
    Papers, Please                                             2020-03-19

.. _Taskwarrior: https://taskwarrior.org/
