####################################################
Geração de listas de reprodução de música com o MPD
####################################################

:date: 2020-09-20
:tags: mpd, python


Música é vida. Eu realmente amo ouvir música, apesar que não o mesmo tipo de
música o tempo todo. Mas na maioria das vezes, vai de tudo: Eu gosto de ouvir
qualquer uma das músicas que eu tenho aleatoriamente. Mas quando eu estou
fazendo algo que precisa de concentração (como escrever esse texto) eu só posso
ouvir música "de fundo", ou seja, música que não tem vocal. Por isso ter
agrupamentos de músicas é bem útil para esse tipo de situação.

Um jeito de agrupar músicas é por meio de listas de reprodução. Mas os
critérios que determinam quais músicas vão em uma lista é bastante subjetivo já
que é determinado por mim, um ser humano. Foi isso que me motivou a criar algum
jeito de gerar listas de reprodução baseado em critérios variados: nome de
pasta, artista, álbum, etc.

Recentemente eu li o incrível livro `Python Fluente`_ (de autor brasileiro!) com
o objetivo de aprofundar minhas habilidades em python, que se tornou minha
linguagem de programação preferida já há algum tempo. Eu aprendi bastante lendo
esse livro, mas ainda não tinha praticado nada do que aprendi. Uma das coisas
mais interessantes que aprendi foi sobre os `métodos especiais`_ que fazem dos
objetos em python tão flexíveis. Foi aí que percebi que usando métodos
especiais, eu conseguiria criar objetos de lista de reprodução bem flexíveis e
ao mesmo tempo praticar esse conceito mágico. Ganha-ganha.

O programa que eu uso para gerenciar minha biblioteca de músicas e também para
tocar as músicas é o MPD_. Ele suporta requisição de informações sobre as
músicas por outros programas e também reproduzir músicas com base em uma lista
de reprodução, então foi simples fazer o código de geração de listas de
reprodução usando o MPD.

A classe MPDPlaylist
====================

A ideia é a seguinte: Eu quero poder criar uma lista de reprodução especificando
o que ela deve conter, e também o que ela não deve, e também poder combinar
critérios de outras listas usando operações de E e OU. Isso provavelmente
ficará mais claro mais à frente com os exemplos.

O código que implementa essa classe está em ``mpd_playlist.py``, e contém o
seguinte:

.. include:: {code}/mpd_playlist.py
   :code: python

Aquele trecho com ``MPDClient()`` no começo é necessário para conectar ao banco
de dados do MPD para posteriormente obter todas as informações sobre as músicas.
Ele é provido pelo pacote python-mpd2_.

O método ``__init__()``, que é chamado quando um novo objeto é criado, pode
receber outra lista de reprodução como seu parâmetro ``query`` e nesse caso a
nova lista apenas as músicas da lista passada. Ele também pode receber um
conjunto de músicas (o que eu ainda nem cheguei a usar, mas fazia sentido
suportar). E finalmente, no caso mais comum, pode receber um dicionário contendo
os parâmetros de busca que vão ser usados para filtrar as músicas do MPD. Para
todos os casos, um nome pode opcionalmente ser passado (necessário para que a
lista possa ser salva).

Então, por exemplo, ``músicas_paramore = MPDPlaylist({'artist': 'Paramore'})``
criaria uma lista de reprodução apenas com músicas do Paramore, e
``músicas_paramore2 = MPDPlaylist(músicas_paramore)`` poderia ser usado para
criar uma cópia dessa lista. Esse segundo caso não parece tão útil mas é a base
para processar expressões, como veremos.

``__repr__()`` só é usado para depurar. Ele especifica como o objeto é escrito
na tela, nesse caso, mostrando quais músicas a lista de reprodução contém.

``__or__()`` e ``__and__()`` são chamadas quando duas listas de reprodução são
combinadas com OU (usando ``|``) e com E (usando ``&``), retornando uma lista
que contém a união (músicas das duas listas) e a intersecção (apenas músicas
presentes nas duas) respectivamente. ``__neg__()`` serve para negar a lista
(usando ``-``), fazendo com que os parâmetros de busca especifiquem o que ela
**não** deve conter, e portanto que ela contenha tudo menos o que for indicado.

``query_songs()`` obtém as músicas do MPD com base nos parâmetros de busca e as
salva dentro do objeto.

``write_to_file()`` salva a lista de reprodução em um arquivo ``.m3u`` para que
possa posteriormente ser lido e as músicas reproduzidas pelo MPD. O nome do
arquivo é o que foi fornecido em ``__init__``.

Agora, alguns exemplos.

Minhas listas de reprodução
===========================

Em outro arquivo, ``playlists.py``, eu tenho a definição de todas as minhas
listas de reprodução usando a classe MPDPlaylist:

.. include:: {code}/playlists.py
   :code: python

As primeiras listas criadas são:

* ``saved``, que contém todas as músicas da pasta ``genres``, essencialmente
  minha biblioteca de músicas;

* ``buffer``, com todas as músicas da pasta ``buffer``, que são aquelas que
  ainda estou ouvindo para decidir se gosto ou não;

* ``fvt`` com todas as músicas de pastas começando com ``%``, que são meus
  álbuns favoritos

Em seguida são definidas listas pra cada um dos gêneros. Algumas delas,
``rock``, ``electronic``, ``pop``, ``post_rock`` e ``rap`` são então combinadas
usando o operador OU (``|``) para criar a lista ``common``. Isso significa que
essa lista de reprodução contém as músicas de todas essas listas combinadas.

Aí a lista ``not_soundtrack`` é criada negando a lista ``soundtrack``, então
aquela contém apenas as músicas não presentes nesta.

A lista ``tdg`` possui apenas músicas do artista ``Three Days Grace``.

A última lista, ``background``, combina várias listas definidas anteriormente
como ``classical``, e também listas anônimas como ``PL({'artist':
"Balmorhea"})`` (que são usadas apenas para criar essa lista, e não serão salvas
como listas independentes, portanto não são armazenadas em variáveis e também
não precisam de um nome como parâmetro), bem como remove músicas específicas
como ``PL({'file': "genres/soundtrack/games/Portal - Still Alive.mp3"})``.

Não é a sintaxe mais sucinta de todas, mas também não chega a ser extensa e é
bastante flexível: serviu para tudo que eu precisava customizando as minhas
listas.

Salvando as listas de reprodução
================================

Talvez agora você esteja se perguntando como essas listas de reprodução são
escritas em arquivos se elas são apenas criadas e armazenadas em variáveis. A
resposta é: python é demais 😃. Esse é ``gen_playlists.py``, o código que faz
isso:

.. include:: {code}/gen_playlists.py
   :code: python

Como aquele comentário gentil em cima diz, ``playlists = (pl for pl in
vars(playlists).values() if isinstance(pl, MPDPlaylist) and pl.name)`` obtém
todas as variáveis do tipo ``MPDPlaylist`` de ``playlists.py``, desde que tenham
nome. Aí laço ``for`` itera sobre elas e salva cada uma em um arquivo com seu
nome.

Por fim, eu adicionei uma linha no ``cron`` para executar esse script todo
domingo, atualizando minhas listas de reprodução.

A lista de reprodução 'newest'
==============================

Como nada é perfeito, tem uma lista de reprodução que eu não consegui integrar
nessa infraestrutura e portanto ficou como um script em bash separado 🤢: a
lista ``newest``. Ela contém as últimas 100 músicas adicionadas à minha
biblioteca.

Existem quatro tipos de datas em arquivos de acordo com o ``stat``:

* data de criação do arquivo
* data do último acesso
* data da última modificação
* data da última mudança de estado

Para obter as últimas músicas adicionadas, e não as últimas editadas (às vezes
eu edito os metadados de uma música, e não quero que isso interfira nessa
lista), eu precisava usar a data de criação, mas ela não é suportada pelo MPD,
então é por isso que estou fadado a usar esse script,
``gen_playlist_newest.sh``:

.. include:: {code}/gen_playlist_newest.sh
   :code: shell

.. _Python Fluente: https://www.amazon.com.br/Python-Fluente-Programa%C3%A7%C3%A3o-Concisa-Eficaz/dp/857522462X
.. _métodos especiais: https://docs.python.org/3/reference/datamodel.html#special-method-names
.. _MPD: https://www.musicpd.org/
.. _python-mpd2: https://github.com/Mic92/python-mpd2
