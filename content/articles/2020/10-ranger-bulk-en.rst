#############################
Bulk file editing with ranger
#############################

:date: 2020-10-20
:tags: ranger, python


My file manager of choice is ranger_. It's terminal-based, provides keybind
mapping for everything making me more efficient in navigating my files, and it's
incredibly extensible by enabling the creation of custom commands in **python**.
If that wasn't enough, it also has a ton of other great features (extensible
file preview, tabs, tags, ...). Go check out their `github page`_, seriously.

One very useful feature of ranger is the ``bulkrename`` command. It allows you
to open an editor with the filenames of all selected files and edit them. After
saving, a shell script is generated to make the necessary renaming (and you're
given the chance to review it), and just like magic *\*poof\** you just renamed
a bunch of files simultaneously from the convenience of your favorite text
editor.

Here's an example:

.. image::  {image}/bulkrename.gif
   :alt: Ranger's bulkrename command being used to rename three files in one go

`At asciinema.org <https://asciinema.org/a/366010>`__

Pretty convenient. But when you think about it, it is rather limited. Why only
allow file renaming? This same workflow could be used to edit any file
attribute. One just needs to provide a way to obtain the attribute for each
selected file and to provide the shell script command that changes the attribute
to be what the user changed it to.

The bulk command
================

I recently wanted to modify the *Artist* ID3 tag of multiple mp3 files at once
which motivated me to write the generic version of ranger's ``bulkrename``
command: ``bulk``.

For that I basically copied the code for ``bulkrename`` and generalized the file
attribute retrieval and the attribute change command generation, by calling the
``get_attribute()`` and ``get_change_attribute_cmd()``, respectively, from a
bulk subcommand stored in the ``bulk`` dictionary.

The ``bulk`` command class is as follows:

.. include:: {code}/bulk_class.py
   :code: python

Then, inside this class, I added a class for each of the bulk subcommands that I
wanted to add: ``id3art``, ``id3tit`` and ``id3alb``, which change the ID3 tag
for the *Artist*, *Title* and *Album*, respectively.

For each one of those, I implemented the ``get_attribute()`` and
``get_change_attribute_cmd()`` methods. ``get_attribute()`` receives a ranger's
file object and should return a string with the attribute, in the case of
``id3art``, the ID3 *Artist* tag, which I used the ``eyed3`` python module to
get.  ``get_change_attribute_cmd()`` receives a ranger's file object, the old
attribute (the one returned by ``get_attribute()``) and the new one (the value
edited by the user), and should return a string containing the shell script
command to apply the change made by the user (in the case of ``id3art``, ``eyeD3
-a NewArtist fileName.mp3``).

Finally, I also added an entry for each of those subcommands to the ``bulk``
dictionary, which maps the subcommand name to its object.

Translating all of this to code, this is what I added inside the ``bulk`` class:

.. include:: {code}/bulk_subcommands.py
   :code: python

Here it is in action:

.. image::  {image}/bulk.gif
   :alt: The id3art bulk subcommand being used to change the ID3 artist tag of three mp3 files in one go

`At asciinema.org <https://asciinema.org/a/366013>`__

Since I thought this generic framework for the bulk command could be useful to
everyone using ranger, with each user implementing their own bulk subcommand, I
opened a `pull request`_. It seems the idea was well received, and they even
intend to make ``bulkrename`` simply an alias to a bulk subcommand, so maybe in
the near future you will be able to make your own bulk subcommands using the
**built-in** ``bulk`` command 🙂.

.. _ranger: https://ranger.github.io/
.. _`github page`: https://github.com/ranger/ranger
.. _`pull request`: https://github.com/ranger/ranger/pull/2109
