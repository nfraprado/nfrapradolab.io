###############################################
Creating movie and game lists using Taskwarrior
###############################################

:date: 2020-05-25
:tags: cli, taskwarrior, organization


I like watching movies and playing games, but I also like to keep track of which
movies I've watched and which games I've beaten. I also got into the habit of
rating each movie I watch. For the longest time, though, I have kept track of
these collections informally in my memory or scattered in text files on my
computer.

A couple years ago I started trying to keep myself more focused and organized by
using a *CLI* (Command-line interface) TODO list manager called Taskwarrior_.
Suddenly I realized that I could also use this awesome tool to keep lists for my
movies and games!

Taskwarrior configuration
=========================

If you know Taskwarrior, you know that it just needs a configuration
file (normally a ``.taskrc`` in your home folder), and a folder to keep its data
files.

In order to keep these lists separate from my main Taskwarrior tasks, I created
a directory (``/home/nfraprado/txt/todo``) to keep a separate configuration file
(``config``) and data folder (``data``).

Inside ``config``, I first defined the data folder:

.. code-block:: ini

    data.location = /home/nfraprado/txt/todo/data

I then removed color from tagged tasks and made search case insensitive:

.. code-block:: ini

    color.tagged = none

    search.case.sensitive = no

And also created a new *UDA*, or User Defined Attribute, called Score, so that I
could add a score to a movie after I watched it:

.. code-block:: ini

    uda.score.type = numeric
    uda.score.label = Score

Finally, I added the movies and games reports. Each report can have its filter
configured to determine which tasks are shown, and its columns, to determine
which task attributes are displayed, as well as some other configurations.

For the games, I wanted two reports: one called ``games.todo``, to show which
games I still need to beat, and another called ``games.done`` for the games I've
already beaten:

.. code-block:: ini

    # Games reports
    report.games.todo.description = Games to complete
    report.games.todo.labels = ID,Title
    report.games.todo.columns = id,description
    report.games.todo.filter = status:pending limit:page project:games

    report.games.done.description = Games completed
    report.games.done.labels = Title,Completed on
    report.games.done.columns = description,end
    report.games.done.filter = status:completed limit:page project:games
    report.games.done.sort = end-

For the movies, I also added a ``todo`` and a ``done`` report, but the ``done``
report also has a score column so that I can see the score that I gave to each
movie. I also added a ``movies.rank`` report for the movies that is sorted based
on the score, so I can have a list of my favorite movies 🙂:

.. code-block:: ini

    # Movies reports
    report.movies.todo.description = Movies to watch
    report.movies.todo.labels = ID,Tags,Title
    report.movies.todo.columns = id,tags,description
    report.movies.todo.filter = status:pending limit:page project:movies

    report.movies.done.description = Movies seen
    report.movies.done.labels = Title,Tags,Score,Watched on
    report.movies.done.columns = description,tags,score,end
    report.movies.done.filter = status:completed limit:page project:movies
    report.movies.done.sort = end-

    report.movies.rank.description = Movies ranking
    report.movies.rank.labels = Title,Tags,Score
    report.movies.rank.columns = description,tags,score
    report.movies.rank.filter = status:completed limit:page project:movies
    report.movies.rank.sort = score-

Bash configuration
==================

With Taskwarrior configured, I just needed some aliases in my ``.bashrc`` to
make it convenient to add, complete and see the movies and games.

Inside my ``.bashrc``, I first added a variable pointing to the config location:

.. code-block:: bash

    TODOCONFIG="$HOME/txt/todo/config"

Then I added aliases to add, list and list done for both games and movies. Also
an alias to mark a game as completed and one to see the movies ranking. For all
these commands, I used ``rc:$TODOCONFIG`` to make Taskwarrior use the
configuration file that I previously added. For the add and done aliases I just
use the ``add`` and ``done`` Taskwarrior commands, respectively. Listing is done
by just calling the previously written reports:

.. code-block:: bash

    # Games list
    alias gamadd='task rc:$TODOCONFIG add project:games'
    alias gamlist='task rc:$TODOCONFIG games.todo'
    alias gamlistdone='task rc:$TODOCONFIG games.done'
    alias gamdone='task rc:$TODOCONFIG done'

    # Movies list
    alias movadd='task rc:$TODOCONFIG add project:movies'
    alias movlist='task rc:$TODOCONFIG movies.todo'
    alias movlistdone='task rc:$TODOCONFIG movies.done'
    alias movrank='task rc:$TODOCONFIG movies.rank'

Completing a movie can't be done as a simple alias, since I also want to give it
a score, so I create a simple bash function that takes the task id and score,
applies the given score to the task and marks it as done:

.. code-block:: bash

    movdone()
    {
        if [ -z "$1" ] || [ -z "$2" ];then
            echo "Usage: movdone id score"
            return 1
        fi
        task rc:$TODOCONFIG modify "$1" score:"$2"
        task rc:$TODOCONFIG done "$1"
    }

Result
======

With this setup done, I could finally track my movies and games very easily.

Adding a new movie to watch is as simples as ``movadd movie_name`` (spaces can
actually be used in the name).

And after watching a movie, I can easily mark it as completed and rate it with
``movdone id rating``. The ``id`` for the movie can be seen using ``movlist``.

Here are the movies that I plan to watch::

    [nfraprado@ArchWay ~]$ movlist
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    ID Title
    23 Once Upon a Time In Hollywood
    27 Ready player one
    28 In The Shadow Of The Moon 2007

The last 5 movies I've watched::

    [nfraprado@ArchWay ~]$ movlistdone
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    Title                                             Tags   Score Watched on
    Arrival                                                      8 2020-05-25
    The Green Mile                                               8 2020-05-17
    The Boy Who Harnessed the Wind                               8 2020-04-25
    Jojo Rabbit                                                  6 2020-04-18
    Back to the Future                                           8 2020-04-12

My highest rated movies (since I created this list)::

    [nfraprado@ArchWay ~]$ movrank
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    Title                                             Tags   Score
    The Lord of the Rings: The Return of the King               10
    The Lord of the Rings: The Two Towers                       10
    The Lord of the Rings: The Fellowship of the Ring           10
    Whisper of the Heart                              ghibli     9
    Apollo 11                                         doc        9
    Life is beautiful                                            9
    Koe no katachi                                               9

Some games I plan to beat::

    [nfraprado@ArchWay ~]$ gamlist
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    ID Title
     6 FTL
     7 The Witness
     8 Factorio
     9 Stardew Valley
    10 Half Life

And the last 5 games I've beaten::

    [nfraprado@ArchWay ~]$ gamlistdone
    Using alternate .taskrc file /home/nfraprado/txt/todo/config

    Title                                                      Completed on
    The Stanley Parable                                        2020-04-24
    Undertale                                                  2020-04-17
    Cave Story                                                 2020-03-30
    Antichamber                                                2020-03-22
    Papers, Please                                             2020-03-19

.. _Taskwarrior: https://taskwarrior.org/
