######################################
Edição de arquivos em massa com ranger
######################################

:date: 2020-10-20
:tags: ranger, python


Meu gerenciador de arquivos é o ranger_. Ele é de terminal, permite remapear
todos os comandos me permitindo ser mais eficiente em navegar pelos meus
arquivos, e é incrivelmente extensível já que permite a criação de comandos
customizados em **python**. Se isso não bastasse, ele também tem um monte de
outras funcionalidades (visualização de arquivos extensível, abas, rótulos,
...). Vá checar a `página do GitHub`_ deles, sério.

Uma funcionalidade muito útil do ranger é o comando ``bulkrename``. Ele permite
que você abra um editor com o nome de todos os arquivos selecionados e os edite.
Depois de salvar, um script de shell é gerado para realizar as renomeações
necessárias (e te dá a chance de revisá-lo), e como se fosse mágica *\*puff\**
você acabou de renomear um monte de arquivos simultaneamente da conveniência do
seu editor de texto preferido.

Veja um exemplo:

.. image::  {image}/bulkrename.gif
   :alt: Comando bulkrename do ranger sendo usado para renomear três arquivos de uma só vez

`Em asciinema.org <https://asciinema.org/a/366010>`__

Bem conveniente. Mas se você parar para pensar, é bem limitado. Por que permitir
só renomeação? O mesmo funcionamento poderia ser usado para editar qualquer
atributo do arquivo. Só é necessário fornecer uma forma de obter o atributo para
cada arquivo selecionado e de gerar um comando de shell que mude o atributo para
aplicar a mudança feita pelo usuário.

O comando bulk
==============

Recentemente eu quis modificar o rótulo ID3 *Artista* de múltiplos arquivos mp3
ao mesmo tempo o que me motivou a escrever a versão genérica do ``bulkrename``
do ranger: ``bulk``.

Para isso eu basicamente copiei o código do ``bulkrename`` e generalizei a
obtenção do atributo do arquivo e a geração do comando de modificação do
atributo, chamando ``get_attribute()`` e ``get_change_attribute_cmd()``,
respectivamente, de um subcomando bulk armazenado no dicionário ``bulk``.

A classe do comando ``bulk`` é a seguinte:

.. include:: {code}/bulk_class.py
   :code: python

Então, dentro dessa classe, eu adicionei uma classe para cada um dos subcomandos
bulk que eu queria adicionar: ``id3art``, ``id3tit`` e ``id3alb``, que modificam
o rótulo ID3 para o *Artista*, *Título* e *Álbum*, respectivamente.

Para cada um deles, eu implementei os métodos ``get_attribute()`` e
``get_change_attribute_cmd()``. O método ``get_attribute()`` recebe o objeto de
arquivo do ranger e deve retornar uma string com o atributo (no caso do
``id3art``, o rótulo ID3 *Artista*, o qual foi obtido usando o módulo python
``eyed3``). O método ``get_change_attribute_cmd()`` recebe o objeto de arquivo
do ranger, o atributo antigo (o retornado por ``get_attribute()``) e o novo (o
valor editado pelo usuário), e deve retornar uma string contendo o comando de
shell para aplicar a mudança feita pelo usuário (no caso do ``id3art``, ``eyeD3
-a NovoArtista nomeDoArquivo.mp3``).

Finalmente, eu também adicionei uma entrada para cada um desses subcomandos no
dicionário ``bulk``, que mapeia o nome do subcomando ao seu objeto.

Traduzindo tudo isso para código, foi isso o que eu adicionei dentro da classe
``bulk``:

.. include:: {code}/bulk_subcommands.py
   :code: python

E aqui está ele em ação:

.. image::  {image}/bulk.gif
   :alt: O subcomando bulk id3art sendo usado para mudar o rótulo ID3 Artista de três arquivos mp3 de uma vez só

`Em asciinema.org <https://asciinema.org/a/366013>`__

Já que eu percebi que esse funcionamento genérico do comando bulk poderia ser
útil para todos os usuários do ranger, cada um implementando seu próprio
subcomando bulk, eu `sugeri a adição da funcionalidade`_. Aparentemente a ideia
foi bem aceita, e inclusive há a intenção de tornar o ``bulkrename`` apenas um
apelido para um subcomando bulk, então talvez em um futuro próximo você possa
criar seu próprio subcomando bulk usando o comando ``bulk`` já integrado ao
ranger 🙂.

.. _ranger: https://ranger.github.io/
.. _página do GitHub: https://github.com/ranger/ranger
.. _sugeri a adição da funcionalidade: https://github.com/ranger/ranger/pull/2109
