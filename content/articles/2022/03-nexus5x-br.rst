#################################
Rodando LineageOS no meu Nexus 5X
#################################

:date: 2022-03-30
:tags: nexus5x, lineageos


Esse mês completa um ano desde que eu comprei um Nexus 5X e comecei a usar
LineageOS, então parece um bom momento para compartilhar minha experiência.

Compra e instalação
===================

Aproximadamente um ano atrás, `um amigo`__ me disse que tinha achado uma oferta
muito boa para um Nexus 5X online. O Nexus 5X já é um celular antigo, lançado em
2015, e por isso tende a ser bem barato, apesar de ainda ser um celular bem
decente mesmo para os padrões atuais. A oferta que ele encontrou era para um
Nexus 5X *novo*, que vinha dentro da caixa original, perfeitamente conservado, e
ainda sim custava apenas R$ 300. Um ótimo negócio.

.. __: https://andrealmeid.com/

Nesse ponto eu já estava usando Linux no meu computador há vários anos, mas meu
celular, possivelmente a máquina que tem acesso à maior quantidade de
informações pessoais, ainda estava rodando Android, que é um SO que não é
completamente FOSS, e isso me incomodava bastante.

Eu já tinha tido vontade de mudar para o LineageOS, a alternativa FOSS ao
Android, há algum tempo, mas como era meu único celular parecia muito arriscado.
Com a possibilidade de ter um novo Nexus 5X de repente eu vi a oportunidade de
ter um celular bem testado (e amado) com LineageOS, e se tudo desse errado, meu
celular antigo ainda estaria lá com tudo exatamente do mesmo jeito. Então eu
decidi pegar um Nexus para mim também.

Eu segui o `guia de Nexus 5X do meu amigo`__ e tive um pouco de ajuda pessoal
dele para entender os componentes de software necessários para rodar uma
instalação de LineageOS totalmente sem Google. No meu caso o celular era novo,
então eu pude usar uma imagem sem o patch de BLOD e ter acesso à performance
total do Nexus 5X.

.. __: https://andrealmeid.com/post/2020-11-23-bullhead-los/

A instalação em si foi fácil de seguir (tudo bem que eu tinha acesso a um guia
e um mentor). E ao final eu tinha meu próprio celular com LineageOS, MicroG e
Magisk, completamente sem Google e ainda sim capaz de instalar software que
dependesse do Google Services quando necessário.

Problemas
=========

No geral a experiência é ótima, mas tem dois problemas principais:

* **Problemas frequentes na câmera**: Basicamente toda vez que eu abro a câmera,
  ela fecha na primeira vez, e eu preciso abrir de novo e aí ela funciona.
  Irritante, mas ainda não o suficiente para eu ir tentar consertar.
* **Suporte de GPS errático**: O GPS foi a coisa mais difícil de fazer
  funcionar. Eu lembro de ter visto algumas pistas `nesse artigo`__, e
  finalmente consegui fazer funcionar simplesmente habilitando os módulos de
  localização "Mozilla Location Service" e "WiFi Location Service", mas nem
  sempre funcionava e demorava um tempo inicial antes de começar a funcionar. E
  aí eu mudei de país e agora não consigo mais fazer funcionar...

.. __: https://blog.eowyn.net/unifiednlp/

Mas esses são problemas de software que podem eventualmente ser consertados. Tem
uma única limitação de hardware no Nexus 5X que me deixou decepcionado assim que
eu notei: **Ele só tem 16GB de armazenamento interno e não tem entrada para
cartão SD**.

Para os aplicativos, 16GB na verdade é até ok, mas para tirar fotos ou vídeos e
armazenar música, não era o suficiente.

Procurando por mais espaço
--------------------------

Mas eu não desisti logo de cara. O celular estava tão perto de ser perfeito, eu
precisava achar um jeito de contornar a falta de espaço de armazenamento.

O Nexus 5X tem uma porta USB-C OTG, então eu percebi que poderia comprar um
pendrive USB-C, o menor possível para que não fosse muito intrusivo, para
efetivamente expandir o espaço de armazenamento.

Depois de pesquisar um pouco, o pendrive USB-C mais compacto que eu encontrei
foi o `Kingston DTDUO3C/128GB`__. Então foi ele que eu comprei. Logo que ele
chegou eu notei que ele vinha com uma cordinha para chaveiro e a capa do meu
celular tinha uma cavidade para isso, então eu amarrei ele lá e achei que ficou
bonitinho. Dito isso tem algumas desvantagens: o pendrive as vezes engacha no
meu bolso, e também bate na tela. Ficou assim:

.. __: https://www.amazon.com/Kingston-Digital-32GB-Traveler-DTDUO3C/dp/B01M59PHE4

.. image:: {image}/usb_disconnected.jpg

.. image:: {image}/usb_connected.jpg

Mas assim que eu inseri o pendrive eu fiquei desapontado ao ver que o tocador de
música não reconhecia nenhuma música que estava lá. Aparentemente o pendrive
estava funcionando apenas como um simples armazenamento e eu só conseguia mover
arquivos de/para ele usando o gerenciador de arquivos.

Mas como isso era um problema estranho, eu pensei que deveria ter um jeito de
contorná-lo. Afinal, era só uma limitação arbitrária imposta pelo Android.
Depois de um pouco de pesquisa, de fato, `um artigo no fórum do XDA veio para o
resgate`__ com uma solução de praticamente uma linha:

.. __: https://forum.xda-developers.com/t/enable-apps-to-read-otg-usb-contents-in-marshmallow-nougat.3539389/

.. code-block:: shell

   su
   sm set-force-adoptable true

E só com isso, reinserindo o pendrive fez o tocador de música rapidamente
começar a indexar todos os arquivos de música que ele tinha acabado de encontrar
no pendrive. Funcionou!

Mas tem um grande porém: eu preciso sempre lembrar de inserir o pendrive antes
de abrir o tocador de música, e fechar ele antes de remover o pendrive. Caso
contrário o tocador não enxerga mais nenhuma música no dispositivo, e na próxima
vez que eu inserir o pendrive, ele demora alguns minutos indexando todos os
arquivos de novo (e travando algumas vezes durante o processo).

Dado que eu precisava que meus arquivos de música (e possivelmente fotos) fossem
sincronizados com o computador, eu também estava interessado em fazer o
Syncthing funcionar com pastas no pendrive.

Para a minha surpresa isso não funcionou de primeira: O Syncthing só tem
permissão de leitura em armazenamentos externos. Mas eu logo descobri que dando
permissão de root para ele remove essa limitação, e isso pode ser feito pelo
Magisk. Não é a solução ideal, mas pelo menos com isso eu tenho uma configuração
funcional para sincronizar e tocar músicas no celular!

Eu também estava curioso para ver se eu conseguiria fazer a câmera salvar fotos
e vídeos diretamente no pendrive. Isso resolveria a maior parte do problema de
pouco espaço no celular.

Para não alterar as configurações do aplicativo de câmera principal, eu baixei o
aplicativo Simple Camera e configurei ele para salvar em uma pasta no pendrive.
Para minha surpresa ele simplesmente pediu permissão de escrita para o pendrive,
e não precisou ser rodado como root como o Syncthing.

Então eu tentei gravar um vídeo com ele. Na primeira tentativa ele deu um erro e
pediu para escolher a qualidade de imagem e eu abaixei para 1080p. Eu acho que a
velocidade de escrita do USB não era rápida o suficiente para a resolução
original da câmera.

Com isso feito, eu consegui tirar fotos e vídeos. Logo depois que eu paro a
gravação do vídeo o aplicativo fecha, mas o arquivo parece ser corretamente
escrito no pendrive e sincronizado com o computador. 

Mas no fim até o momento eu tenho evitado usar o pendrive para a câmera (usando
o aplicativo de câmera original), porque, diferentemente de música, tirar
fotos/vídeos é situacional, e eu não quero arriscar que minhas fotos falhem ao
salvar ou precisem de múltiplas tentativas (já basta o aplicativo travando...).
De qualquer forma é bom saber que ele fuciona, até certo ponto, para quando eu
quiser usar a câmera e o armazenamento interno estiver cheio.

Pendrive como armazenamento interno
...................................

Mas algo mais aconteceu quando eu habilitei a configuração ``force-adoptable``.
Navegando nas configurações, aparentemente que agora eu podia formatar o
pendrive como um armazenamento interno. Isso faria com que a memória do pendrive
fosse usada transparentemente, até para armazenar os aplicativos instalados. Mas
uma pergunta veio à mente: "O que aconteceria quando eu removesse o pendrive?".
Só tinha um jeito de descobrir se isso seria usável ou não...

Levou em torno de 30 minutos para o pendrive ser formatado e a maioria dos dados
do armazenamento interno ser movido para ele. Mas no final eu tinha extendido o
armazenamento do meu celular em 128GB com sucesso. E a sensação foi de... muita
lentidão. O sistema inteiro ficava dando travadas o tempo todo. Pelo visto a
velocidade do pendrive é muito mais lenta do que a do armazenamento interno. E
agora que o sistema dependia dessas transferências isso ficava bem aparente.

Mas talvez eu conseguisse viver com um celular lento em troca de mais espaço de
armazenamento, né? E a grande pergunta: o que acontece quando o pendrive é
removido? Isso é muito importante de saber já que eu precisaria removê-lo no
mínimo para carregar uma vez por dia. E a resposta é: ele não gosta nem um pouco
disso. O sistema reinicia 😝.

Eu passei quase três dias assim: Com um celular com um armazenamento gigante de
128GB, mas que era lento e reiniciava toda vez que eu precisava recarregá-lo (o
que também acontecia mais frequentemente já que o pendrive aparentemente consome
bastante bateria).

No final desses três dias eu já estava cansado disso e então reformatei o
pendrive como armazenamento externo e voltei para a configuração anterior. Foi
um experimento interessando (apesar de frustrante).

Recomendações de aplicativos
============================

Em questão de aplicativos, eu tento o máximo possível usar aplicativos da loja
F-Droid_, mas tem alguns aplicativos que eu ainda preciso usar da PlayStore.
Felizmente, o aplicativo AuroraStore, que já vem instalado, pelo menos fornece
uma alternativa privativa à PlayStore.

.. _F-Droid: https://f-droid.org/

Dos aplicativos que eu instalei da F-Droid, esses são os que eu realmente gosto:

* AntennaPod_: Tocador de podcast.
* DiskUsage_: Visualizador de uso de espaço de armazenamento.
* EtchDroid_: Criador de imagem USB. Eu falei sobre ele no `<{article}etchdroid>`__.
* Infinity_: Visualizador do Reddit.
* KeePassDX_: Gerenciador de senhas.
* Mullvad_: Cliente VPN.
* NewPipe_: Visualizador do YouTube.
* `OsmAnd~`_: Visualizador de mapa offline baseado no OpenStreetMap.
* Pluvia_: Visualizador de previsão do tempo.
* Puzzles_: Uma coleção de bons jogos de puzzle.
* Syncthing_: Sincronizador de arquivos.
* Termux_: Terminal.
* Vinyl_: Tocador de música.

.. _AntennaPod: https://f-droid.org/en/packages/de.danoeh.antennapod
.. _DiskUsage: https://f-droid.org/en/packages/com.google.android.diskusage
.. _EtchDroid: https://f-droid.org/en/packages/eu.depau.etchdroid
.. _Infinity: https://f-droid.org/en/packages/ml.docilealligator.infinityforreddit
.. _KeePassDX: https://f-droid.org/en/packages/com.kunzisoft.keepass.libre
.. _Mullvad: https://f-droid.org/en/packages/net.mullvad.mullvadvpn
.. _NewPipe: https://f-droid.org/en/packages/org.schabi.newpipe
.. _OsmAnd~: https://f-droid.org/en/packages/net.osmand.plus/
.. _Pluvia: https://f-droid.org/en/packages/com.spicychair.weather
.. _Puzzles: https://f-droid.org/en/packages/name.boyle.chris.sgtpuzzles/
.. _Syncthing: https://f-droid.org/en/packages/com.nutomic.syncthingandroid
.. _Termux: https://f-droid.org/en/packages/com.termux
.. _Vinyl: https://f-droid.org/en/packages/com.poupa.vinylmusicplayer

Conclusão
=========

Eu não sei por quanto tempo vou continuar usando esse celular, mas eu com
certeza não volto mais para o Android. Eu vou ou continuar com o LineageOS or
talvez me aventurar no PostmarketOS, se a experiência de usuário estiver num
nível bem usável até lá (ou não se eu sentir que de repente tenho bastante tempo
livre 😝). De qualquer forma eu vou garantir que meu próximo celular tenha mais
espaço de armazenamento 🙂.
