#######################################
Discovering the comfort of loudspeakers
#######################################

:date: 2022-05-27
:tags: buy, comfort, sound


In 2014, while on a school trip to Germany, I bought a Razer Tiamat headset. The
sound was great and for the most part it was comfortable. Its only issue was the
clamping force, which was a bit too much, and gave me headaches after long hours
of use.

I made great use of this headset and it served me all the way up to 2021.
By that point, the leather on the earpads was mostly gone, and the jack
connector had already broken twice and needed resoldering a new one. One of the
times the contact on one of the wires wasn't very good, which caused one side
to be louder than the other and needed compensating in software, but then the
compensation needed changed with changes in volume, which drove me crazy and I
remember being incredibly happy when I finally fixed it.

So by 2021 a new headset was already overdue. Since the clamping force was my
main pain point (quite literally), I was going to try to find one that had the
lowest possible while still being reasonably priced.

Searching for a comfortable headset
===================================

Searching online I found a site called Rtings that reviewed and ranked
headphones, with one of the criteria being the clamping force. I filtered their
list by this criterion, and settled on the HyperX Cloud Alpha since it had one
of the lowest clamping force and wasn't very expensive.

But when it arrived, I was very disappointed. The clamping force was much higher
than what I had on the Tiamat and I easily got headaches after just a couple
hours using it. I tried flexing it overnight, but that didn't seem to make a
difference. So I eventually just started avoiding to use the headset, and used
the builtin notebook speakers instead, which sound awful, but they at least
aren't painful.

A few months later I had moved to the US and had a much wider and accessible
array of options at my disposal, so I decided to try it again. I wasn't going to
get it wrong a second time, so I made sure to try out as many headphones as I
could at physical stores, B&H being the biggest one I knew.

To my surprise however, almost all of them had a big enough clamping force
to be uncomfortable for me. Even the Bose QuietComfort 35, which from my
research was one of the most comfortable headphones available, was a bit too
tight for me. I did try some that were actually okay, like Sony Mdr7506,
Phillips HP9500 and Koss UR20. And funnily enough they were also cheaper than
most others. But these headphones seemed like they were so loose they were about
to fall off. There didn't seem to be a sweet spot for the clamping force.

At this point, I started thinking that maybe my problem was inherent to
headphones. I had already thought of earbuds, but the issue with these is that
they deform inside your ear, causing a different kind of pain. Then I suddenly
thought about loudspeakers. Could they be my answer?

Searching for a good speaker
============================

As I researched I found a very well researched and tested `Speakers ranking from
Wirecutter`__. Looks like this is one of the most well known series of rankings,
but I hadn't come across it before so I was happy to find it.

.. __: https://www.nytimes.com/wirecutter/reviews/best-computer-speakers/

I came very close to ordering one of their top picks, but I kept feeling like a
subwoofer would be more important to me than the flat response they were
prioritizing, since my use case is almost exclusively listening to music, and
not producing.

They did have as their "Also great" the Klipsch ProMedia 2.1 THX, which has a
subwoofer. As I researched around, I noticed that while other rankings had
different top picks, this speaker was also consistently appearing on other
ranks. And it seems like this speaker has been around for a long time, so if
it's still relevant in these rankings, then it must be really good.

The downsides for the Klipsch usually mentioned the need for extra space for its
bulky subwoofer and lack of conectivity eg. bluetooth - the audio input is
exclusively through a 3.5mm jack cable. I didn't have any problem with the extra
space needed: The subwoofer goes on the floor, and I have plenty of free space
there! As for the jack input only: I was going to keep it constantly
connected to the Line Out port on my docking station, so no worries here either.

Finally, to get a better feeling for how important a subwoofer would be for me,
I opened some songs that I liked and that had a deep bass in them in
Audacity and used the Filter Curve effect to try and imitate the frequency
response graph that the Wirecutter article showed for the speakers with no
subwoofer to try and see how it would sound like. This proved very useful and it
was clear to me that without the lower frequencies, these songs lost a lot of
their substance. So I concluded that I really wanted a subwoofer, and the
Klipsch seemed like the best speaker with one, so I went for it.

The Klipsch ProMedia 2.1 THX
============================

And oh boy... This has to be one of my happiest purchases in life. First of all,
because of the change from headphone to loudspeaker. I can't believe I can
finally listen to music *all day long* without feeling anything *at all* on my
head. This is *magic*.

Second, the subwoofer really brings the music to life. Not *every* song makes
use of it, but more songs than I thought. You can mainly hear it (and feel it!)
from the bass and the percussion in the songs. There is a subwoofer dial and I
keep turning it down and up again to compare the difference it makes on the
sound and it's impressive.

As for the downsides, there's a bright power LED between the dials and no ON/OFF
switch, which means this LED is always on. But a small piece of tape to cover it
easily solves the issue.

Another thing is that by having such a good speaker fixed to my desk, I do
miss it when I want to take my notebook to a different room. If I could somehow
bring it with me in a portable way, that would be wonderful... But then we're
back at the concept of headphones and we already know where that path leads 😝.
I also miss a bit the sound isolation (in both ways) that you get from
headphones, but that's way less important than comfort to me.

Conclusion
==========

I find it interesting that when I was a kid, the computer in my house had a pair
of speakers as was usual. But since then I've only had, and seen other people
use, headphones, so I subconsciously considered it the only option. Realizing
that's not the case, even though loudspeakers do seem a lot less common
nowadays, and going back to them now feels poetic in a way.

I do still have my hated headset and I use it for calls, since the echo from the
speakers would be very bad. But as soon as the call is over I'm *really* happy
to get the headset off and get back to the comfort of my speakers 😌.
