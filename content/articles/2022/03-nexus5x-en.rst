################################
Running LineageOS on my Nexus 5X
################################

:date: 2022-03-30
:tags: nexus5x, lineageos


This month marks one year since I bought a Nexus 5X and started using LineageOS,
so I thought I'd share my experience.

Setting up
==========

About one year ago, `a friend`__ told me he found a really good deal for a Nexus
5X online. The Nexus 5X is an old phone by now, launched in 2015, so it tends to
be fairly cheap, even though it's still a rather decent phone by today's
standards. The offer he found was for a *new* Nexus 5X, which came inside the
original box, perfectly conserved, and still cost only R$ 300. Really good deal.

.. __: https://andrealmeid.com/

At this point I had already been using Linux on my computer for some years, but
my phone, arguably the machine that has access to the biggest amount of personal
data, was still running Android, which is an OS that is not entirely FOSS, and
that bothered me quite a bit.

I had already had the desire to switch to LineageOS, the FOSS alternative to
Android, for some time now, but since it was my only phone it felt like putting
too much at stake. With the possibility of getting a new Nexus 5X suddenly I saw
the opportunity of having a tried and tested (and loved) phone with LineageOS,
and if all went wrong, my older phone would still be there with everything the
same way it was. So I decided to get a Nexus for me as well.

I followed `my friend's Nexus 5X guide`__ and had some personal help from him in
order to understand the required software components to be able to run a
LineageOS installation completely de-Googled. In my case the phone was brand new
so I could use the image without the BLOD patch and enjoy full Nexus 5X
performance.

.. __: https://andrealmeid.com/post/2020-11-23-bullhead-los/

The installation itself was easy to follow (granted I had a guide and a mentor).
And at the end I had the phone with LineageOS, MicroG and Magisk, completely
de-Googled and yet able to install software that relied on Google Services when
needed.

Issues
======

Overall the experience is great, but there are two main issues:

* **Frequent camera crashes**: basically every time I open the camera, it
  crashes on the first try, and I need to reopen it and then it works. Annoying,
  but not enough yet for me to go fix it.
* **Flaky GPS support**: GPS was the hardest thing to get working. I remember
  finding some pointers `in this post`__, and finally got it working by simply
  enabling the "Mozilla Location Service" and "WiFi Location Service" location
  modules, but it didn't always work and would have an initial delay before it
  started working. And then I moved countries and can't get it working at all
  anymore...

.. __: https://blog.eowyn.net/unifiednlp/

But those are software issues and could be eventually fixed. There is however
one single hardware limitation on the Nexus 5X that made me disappointed as soon
as I realized: **There's only 16GB of internal storage and no external storage
card slot**.

For the apps themselves 16GB can actually be ok, but for taking pictures or
videos and storing music, it wasn't enough.

Searching for more space
------------------------

But I didn't give up immediately on the matter. The phone was so close to
perfect, I needed to find a workaround for the lack of storage space.

The Nexus 5X has an USB-C OTG port, so I realized I could buy a USB-C
flash drive, the smallest possible so that it wouldn't be too intrusive, in
order to effectively expand the storage space.

After researching for a bit, the most compact USB-C flash drive I found was the
`Kingston DTDUO3C/128GB`__. So I went ahead and bought it. Right after it
arrived I noticed that it comes with a little keyring and that my phone case had
a slot for that, so I tied it there and thought it looked cute. It's also way
easier to keep it always together with the phone this way. That said there are
some drawbacks: the USB drive sometimes gets stuck in my pocket, and also hits
the screen. This is how it looks:

.. __: https://www.amazon.com/Kingston-Digital-32GB-Traveler-DTDUO3C/dp/B01M59PHE4

.. image:: {image}/usb_disconnected.jpg

.. image:: {image}/usb_connected.jpg

However, as I inserted the flash drive I was disappointed to see that my music
player didn't recognize any of the music in there. It seemed that the flash
drive was just like a dumb storage and I could only move files to/from it from
the file manager.

But given that this was weird, I thought there needed to be a way around it.
After all, it's just an arbitrary limitation imposed by Android. After a bit of
research, sure enough, `a post in the XDA forum came to rescue`__ with a
one-liner solution:

.. __: https://forum.xda-developers.com/t/enable-apps-to-read-otg-usb-contents-in-marshmallow-nougat.3539389/

.. code-block:: shell

   su
   sm set-force-adoptable true

And just like that, reinserting the USB flash drive made the music player app
hurringly start indexing all the music files it had just found in the flash
drive. It worked!

There is one big caveat with this however, which is that I need to remember to
always insert the USB drive before opening the music player, and closing it
before removing the drive. Otherwise the player no longer sees any music on the
device, and the next time I plug the drive in, it spends a couple minutes
indexing all the files again (and crashing a few times during the process).

Given that I'd need my music files (and possibly photos) to be synchronized with
the computer, I was also interested in getting Syncthing working with folders
in the USB drive.

To my surprise it didn't work at first: Syncthing has read-only permission to
external storage. But I quickly found out that giving it root permission removes
this limitation, which can be done through Magisk. Not the ideal solution, but
at least this gives a working setup for music!

I was also curious to see if I could have the camera save photos and videos
directly to the USB drive. This would lift most of my concerns about being short
on storage.

In order to not fiddle with the main camera app settings, I downloaded the
Simple Camera app to try this out on, and configured it to save to a folder in
the USB storage. To my surprise it simply asked for permission to have
write-access to the USB, not requiring to be run as root like Syncthing did.

I then tried recording a video on it. On the first try it threw an error and
asked me to select the image quality and I lowered it to 1080p. I think the
write speed on the USB couldn't cope with whatever the original resolution was.

After that though, it did manage to successfully take pictures and record
videos. However right after I stop recording a video the app closes, but the
file does seem to be flushed correctly to the USB storage and sync'ed with my
computer.

But actually so far I have avoided using the USB storage for the camera
(by using the original camera app), because, unlike for music, taking
pictures/videos is time-sensitive, and I don't want to risk having my pictures
fail to save or take multiple tries (the app crashing is enough already). In any
case it's good to know that it works, to some extent, for when I want to use the
camera and the internal storage is full.

USB as internal storage
.......................

But something else happened when I set that ``force-adoptable`` configuration.
While browsing in the settings, it looked like I now could actually format the
flash drive as internal storage. This would mean that the flash drive memory
would be used transparently, even to hold app instalations. But a question came
to mind: "What would then happen when I removed the flash drive?". There was
only one way to find out if this was usable or not...

It took something like 30 minutes for the flash drive to be formatted and most
of the internal storage data to be moved to it. But at the end of that I had
successfully extended my storage by 128GB. And it felt... really sluggish. The
whole system would stutter all the time. Turns out that the USB flash drive
speed is a lot slower than the internal storage. And now that the system
depended on those transfers it was really noticeable.

But maybe I could live with a slower phone in exchange for more storage space,
right? What about the big question: was *does* happen when I remove the USB
drive? This is really important to know given that I'd need to remove it at
least to charge the phone every day or so. And the answer is: the phone doesn't
like it all. The system reboots 😝.

I spent almost three days like this: With a phone with a giant 128GB storage
space, but that was laggy and would reboot every time I needed to recharge it
(which also happened more often as it turns out that the flash drive drains
quite a bit of battery while plugged in).

At the end of those three days I had had enough of it so I reformated the flash
drive as external storage and went back to the previous setup. It was an
interesting (although frustrating) experiment.

App recommendations
===================

App-wise, I try as much as possible to use apps from the F-Droid_ store, but
there are a handful of apps that I still need to use from the PlayStore.
Luckily, there's the builtin app AuroraStore for that, which at least provides a
privacy-friendly version of the PlayStore.

.. _F-Droid: https://f-droid.org/

For the apps that I have installed from F-Droid, here are the ones that I really
like:

* AntennaPod_: Podcast player.
* DiskUsage_: Storage space usage viewer.
* EtchDroid_: USB image creator. I've covered it in the `<{article}etchdroid>`__.
* Infinity_: Reddit viewer.
* KeePassDX_: Password manager.
* Mullvad_: VPN client.
* NewPipe_: YouTube viewer.
* `OsmAnd~`_: Offline map viewer based on OpenStreetMap.
* Pluvia_: Weather forecast viewer.
* Puzzles_: A collection of good puzzle games.
* Syncthing_: File synchronizer.
* Termux_: Terminal.
* Vinyl_: Music Player.

.. _AntennaPod: https://f-droid.org/en/packages/de.danoeh.antennapod
.. _DiskUsage: https://f-droid.org/en/packages/com.google.android.diskusage
.. _EtchDroid: https://f-droid.org/en/packages/eu.depau.etchdroid
.. _Infinity: https://f-droid.org/en/packages/ml.docilealligator.infinityforreddit
.. _KeePassDX: https://f-droid.org/en/packages/com.kunzisoft.keepass.libre
.. _Mullvad: https://f-droid.org/en/packages/net.mullvad.mullvadvpn
.. _NewPipe: https://f-droid.org/en/packages/org.schabi.newpipe
.. _OsmAnd~: https://f-droid.org/en/packages/net.osmand.plus/
.. _Pluvia: https://f-droid.org/en/packages/com.spicychair.weather
.. _Puzzles: https://f-droid.org/en/packages/name.boyle.chris.sgtpuzzles/
.. _Syncthing: https://f-droid.org/en/packages/com.nutomic.syncthingandroid
.. _Termux: https://f-droid.org/en/packages/com.termux
.. _Vinyl: https://f-droid.org/en/packages/com.poupa.vinylmusicplayer

Conclusion
==========

I don't know how long I'll go on using this phone, but I'm surely not going back
to Android. I'll either stay with LineageOS or maybe take the plunge to
PostmarketOS, if the user experience is in the JustWorksTM level by then (or not
if I feel like I suddenly have a bunch of free time 😝). Either way I'm going to
make sure my next phone has a bit more storage space 🙂.
