#########################################
Learning music theory by writing melodies
#########################################

:date: 2021-04-20
:tags: music, music-theory, piano


Hey, first non-technical blog post 🙂.

Anyway, I haven't talked about this before here, but I'm really interested in
playing the piano. When I was younger I took some guitar classes, but I always
wanted to play the piano, and a couple years ago I finally got one! I've been
playing it since, although never really took classes and also didn't practice
very hard, but I do have a lot of fun with it.

What I like the most is improvising. I find it very relaxing, and sometimes
interesting things come up that I want to develop further into a music.

Another thing I'm really interested in is music theory. I find it fascinating
how we can dissect music to get to their building blocks and find out what makes
them sound good. Also, I think if I learn more music theory I can have even more
fun improvising and composing on the piano 😀.

Two great resources I'm using right now to learn music theory are:

* `Dave Conservatoire`__
* `"Approaching Music Theory: Melodic Forms and Simple Harmony" Course on
  Coursera`__

.. __: https://www.daveconservatoire.org/
.. __: https://www.coursera.org/learn/melodic-forms-simple-harmony

Dave Conservatoire is like the Khan Academy for Music Theory and it's really
good! I'm brushing up on the basic concepts as well as learning new ones there.

The Coursera course is what I'm most excited about, because it teaches the
building blocks of music in a way that is fun, and by analyzing existing pieces
of music. And even more, there are assignments in which you need to compose your
own short melody in different styles. And this is what I want to talk about in
this post!

So far through the course I had to make melodies in three different styles:
Gregorian chant, jazz and folk. I tried to compose only by writing on the paper,
but I honestly relied a lot on playing them on the piano to hear how each note
sounded.

After I finished each melody, I transcribed them to a more professional music
sheet using Musescore_, which is an awesome open-source software for writing
music sheet. What is even more awesome is that it can play your music sheet
using MIDI! So for each melody I exported both the music sheet and the audio
playback so I can show them both here.

.. _Musescore: https://musescore.org

My melodies
===========

Gregorian chant
---------------

The idea for the Gregorian chant was to do something using a very limited set of
notes, and few leaps, but as you can see I broke this second rule, although I
think it sounds pretty good.

Being a `Gregorian chant`_, the sheet should only have 4 lines and not have
measures at all, it should have a natural flow. But Musescore isn't really meant
to do that, so that's why both the audio and the sheet are more "robotic" than
I'd like.

.. _Gregorian chant: https://en.wikipedia.org/wiki/Gregorian_chant

`Listen here <{static}/audio/melodies/gregorian_chant.mp3>`__

.. image:: {image}/gregorian_chant.png
   :alt: Gregorian chant melody music sheet

Slow Jazz
---------

Next, in the slow Jazz, there's should be a more strict sense of time, but have
the pitches be more free, so accidentals are even encouraged.

For the accidentals I used both A and D natural, but as you can see, there
aren't any sharp A's to make the natural ones feel off the key signature, as I
later learned. That's definitely something I'd want to improve next time.

I also think I accidentally put too much of the aesthetic of Genshin Impact's
soundtrack, which although is pretty, isn't Jazz 😛.

`Listen here <{static}/audio/melodies/slow_jazz.mp3>`__

.. image:: {image}/slow_jazz.png
   :alt: Slow Jazz melody music sheet

Folk
----

Finally, the Folk melody should be something more repetitive and easy to
remember and sing along. I did it in pentatonic since I learned that worked
well, and I also think having an interesting rhythm is important, so that's what
I did.

`Listen here <{static}/audio/melodies/folk.mp3>`__

.. image:: {image}/folk.png
   :alt: Folk melody music sheet

These are all for now, but I hope to share more in the near future 🙂.
