#####################
Customizações do blog
#####################

:date: 2021-07-24
:tags: blog
:series: Um ano de blog


Eu tenho usado o pelican_ como meu gerador de blog estático desde que comecei
esse blog um pouco mais de um ano atrás. Só precisou de um pouco de configuração
para ter o blog funcionando, e um pouco de pesquisa no pelicanthemes_ para
achar o nikhil-theme_ que é o tema que eu ainda uso.

.. _pelican: https://github.com/getpelican/pelican/
.. _pelicanthemes: http://pelicanthemes.com/
.. _nikhil-theme: https://github.com/gunchu/nikhil-theme

Apesar da configuração base do blog ter sido fácil, ela não ficou perfeitamente
adequada para o meu uso. Foi aí que eu comecei a customizar algumas coisas para
deixar o blog sob medida para mim. Felizmente o pelican é `muito
customizável`__, a ponto de oferecer uma interface para plugins além das
configurações normais. Ele também é escrito em python, então se tudo der errado,
sempre existe a alternativa de hackear um patch para fazer o serviço 😝.

.. __: https://docs.getpelican.com/en/stable/index.html

Então como prometido no artigo anterior, nesse eu vou falar sobre as principais
customizações que eu fiz no meu blog até o momento. Ao longo do texto eu vou
referenciar os commits em que eu fiz as mudanças mencionadas aqui.

Customizações
=============

Plugins do pelican
------------------

As customizações que tiveram mais impacto foram provavelmente os plugins de
pelican que eu adicionei. Eles não são minhas modificações, mas dada sua
importância eu quero mencioná-los mesmo assim.

Os dois plugins que eu estou usando atualmente são i18n_subsites_ e series_, e
foram bem fáceis de encontrar pelo `repositório de plugins`_.

.. _repositório de plugins: https://github.com/getpelican/pelican-plugins/
.. _i18n_subsites: https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites
.. _series: https://github.com/pelican-plugins/series

O i18n_subsites é ótimo para o meu blog com idioma duplo. Por padrão o pelican
suporta apenas traduzir artigos do blog como textos independentes, de forma que
cada artigo que tenha sido traduzido possui links para as traduções. Mas o que
eu realmente queria era ter duas versões do meu blog, uma em inglês e outra em
português. Cada uma com toda a interface na sua própria língua além de apenas
mostrar os artigos dessa língua. Esse plugin possibilita exatamente isso.

É um pouco confuso de entender como configurar corretamente no começo,
principalmente a `parte de localização do template`__, mas bom, eu aprendi sobre
Jinja2 e gettext! Commits relevantes: 1__ (foi parte do commit inicial...), 2__,
3__, 4__ e 5__.

.. __: https://github.com/getpelican/pelican-plugins/blob/master/i18n_subsites/localizing_using_jinja2.rst

.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/f538d7ebf78f9b93dd046d6aa44eaae08b287b66
.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/9a02ff6bd640ea3c79e9e66f72b68116bed17b38
.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/604ca272908e77d50bc459ba51f45e47fbfb30bd
.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/c2effc722cf01b834c7f2373bd6ef8791e4adc7d
.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/d23b1151f12db58a9f9b4bbdbd9389cf6fdf53e4

O series eu adicionei um pouco depois. Ele é muito simples mas útil. A propósito
você pode vê-lo em ação nesse mesmo artigo no topo. Ele é o que me permite fazer
esse artigo ser parte de uma série de artigos, com link para os outros. Commits
relevantes: 1__.

.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/b6f481df611fa1b2e0815e2eeaf2b0a7a8016f26

Feed RSS separado para cada sub-site i18n
-----------------------------------------

Ainda no tema de tradução, já que eu queria sub-sites separados para cada
idioma, também fazia sentido ter um feed RSS para cada um deles. No fim isso
acabou sendo uma mudança de uma única linha, apesar de ter demorado um pouco
para eu descobrir como fazer isso. Commits relevantes: 1__.

.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/dee307122f54e04f9aaa5a203b29a9c029a5f88f

Detecção automática do idioma pelo nome do arquivo
--------------------------------------------------

Outra customização relacionada a tradução (foi algo bem importante pro meu blog!
😝) foi a configuração automática do idioma de um artigo com base no nome do
arquivo. Já que eu já quero que o arquivo para cada tradução de um artigo tenha
o idioma em seu nome, isso me poupa de ter que adicionar o cabeçalho de idioma
também.

Por exemplo, os arquivos para este artigo são chamados
``07-blog-custom-en.rst``, para a versão em inglês, e ``07-blog-custom-br.rst``
para a versão em português. Normalmente eu também precisaria configurar o
cabeçalho de metadados ``lang`` para cada um com o idioma correspondente, mas
com essa mudança ele é configurado automaticamente 🙂. Commits relevantes: 1__
(de novo, parte do commit inicial 😅) e 2__.

.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/f538d7ebf78f9b93dd046d6aa44eaae08b287b66#4cd8c938f4dfb9b2579dcfaf97c623df6ca28eb8_0_23
.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/1712f5b44b2d51de410348462c9d8a66d8b210fa

Configuração de ícone para páginas
----------------------------------

Quando eu estava configurando a barra de navegação no topo, eu queria adicionar
um ícone para cada item, para deixar bem claro o que cada um faz. Mas a página
Sobre, diferentemente dos outros, é uma página gerada do fonte (assim como os
artigos) e adicionada automaticamente à barra, então não é possível configurar
seu ícone direto no HTML.

Para resolver isso eu criei um cabeçalho de metadado customizado ``icon`` que
armazena o nome do ícone FontAwesome que será mostrado na barra para a página.
Aí eu apenas adicionei o nome daquele ícone de "círculo de informação" para a
página Sobre e a aparência ficou bem melhor 🙂. Commits relevantes: 1__.

.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/76534d397a83117327aa1381241b412c77a47b7b

Conserto da renderização de literal no rst
------------------------------------------

Vamos finalizar o artigo com a maior mudança 😝.

Primeiro de tudo, um pouco de contexto: Quando eu comecei o blog, eu escrevia
meus artigos em Markdown. Mas alguns meses depois eu conheci o reStructuredText_
(daqui para frente, chamado de rst) porque ele era usado na documentação do
Kernel Linux, e comecei a usar mais ainda quando descobri o rst2pdf_, que eu não
vou entrar em detalhes já que ainda pretendo escrever sobre ele eventualmente. O
ponto é que eu passei a escrever os meus artigos em rst já que ele também é
suportado pelo pelican.

.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _rst2pdf: https://rst2pdf.org/

Mas um problema muito irritante que eu enfrentei foi que o gerador HTML padrão
do rst traduz blocos literais para tags ``<tt>``. Essa tag foi descontinuada no
HTML5, e o mundo inteiro usa ``<code>`` para código no meio do texto, incluindo
meu tema, o que significa que literais não eram renderizados corretamente.
(`Existem motivos`__ para o rst não usar ``<code>`` para literais, mas para o
meu blog eles não são relevantes)

.. __: https://sourceforge.net/p/docutils/mailman/docutils-develop/thread//p/docutils/bugs/393/de24b5cc7539766427b108223bbcedb8ea8aef5f.bugs%40docutils.p.sourceforge.net/#msg37057965

Eu poderia ter simplesmente adaptado meu tema para contornar esse problema, mas
isso seria apenas escondê-lo: A tag descontinuada ``<tt>`` continuaria ali para
todo o mundo ver.

Em um primeiro momento, eu contornei esse problema usando o *role* ``code``
explicitamente::

    O seguinte é um literal no rst, então ficará dentro de uma tag <tt>: ``bla``
    O seguinte usa o role 'code', então ficará dentro de uma tag <code>: :code:`bla`

Essa é claramente uma sintaxe muito extensa para um blog que frequentemente
usa código no meio do texto, então eu me cansei rapidamente dela. Eu comecei
então a redefinir o *role* padrão para ser ``code``, e a usar um único símbolo
de crase ao invés dos dois que se usa para literais::

    .. default-role:: code

    Agora o seguinte vai usar o role 'code': `bla`

Já que o texto está entre um único símbolo de crase de cada lado mas não possui
um *role* especificado, ele usará o padrão. Muito melhor, mas eu ainda
precisava escrever aquela definição de ``default-role`` no começo de cada
documento...

Finalmente, eu pensei em uma solução ainda melhor: Criar um plugin de leitor de
rst para o pelican que modifique o leitor padrão apenas quando estiver lidando
com literais, para que a saída seja ``<code>`` ao invés de ``<tt>``.

Honestamente isso foi mais fácil do que eu esperava. Só precisou copiar um pouco
dos leitores do pelican e do Sphinx e funcionou perfeitamente! Agora eu
finalmente conseguia escrever literais usando a sintaxe padrão do rst e ver eles
renderizados como ``<code>`` no HTML sem nenhum trabalho a mais 🙂. Commits
relevantes: 1__ e 2__ (esse é apenas eu atualizando todos os textos do blog para
usar a nova sintaxe).

.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/5349ef680c32f124cc425699749498f5debda0a1 
.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io/-/commit/bfb15a96a09ef7dd3403b9ed3e29f89a2a743731

Conclusão
=========

Com essas customizações, eu tenho um blog em que me sinto bastante confortável,
tanto para escrever quanto para ler. Essas mudanças que eu mostrei aqui foram as
que eu achei mais interessantes, mas claro que `todo o código é aberto`__,
então fique à vontade para ver o resto.

.. __: https://gitlab.com/nfraprado/nfraprado.gitlab.io
