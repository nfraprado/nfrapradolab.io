################
One year of blog
################

:date: 2021-06-22
:tags: blog-birthday, blog
:series: One year of blog


It's been a full year since I started this blog! So I thought I'd take this
chance to talk a bit about the blog itself: How it started and my thoughts on
it.

Origins
=======

I had already thought a bit about having my own blog. Having a little corner of
the internet where I'd be free to talk about stuff I'm interested in sounded
really cool. And maybe other people would even find it interesting as well!

But I'm not interested in web development at all, so I just wanted to write my
posts without needing to deal with that. That seems to point to ready-to-use
site generators like WordPress, but I didn't like the sound of that as well.

Luckily, there was already a trend of using static site generators. They were
fast, simple to use and you could version the source in git and host the site
for free using Gitlab/Github! As soon as I learned about them I knew that was
what I wanted.

At the time two of my friends had their own blogs already
(https://rafaelg.net.br and https://andrealmeid.com) using static site
generators and they encouraged me to create one as well. One of them introduced
me to pelican_, a static blog generator in python and I decided to give it a
try.

.. _pelican: https://github.com/getpelican/pelican/

After choosing a theme and a bit of customization in pelican (which I'll cover
in the next post), I was ready to start writing. And that's when I began the
blog with the `<{article}tasklist>`__, which is funnily personal, while still
technical. I like this style 🙂.

So that's how the blog started. Now I want to go a bit further on the principles
that guide the blog.

Principles
==========

There are two things I think are very important about the way my blog works: the
dual-language and the open diary format.

Dual-language
-------------

You might have noticed that all my blog posts have a "translations" field
listing one other language. And there's also a button to change languages on the
navigation bar. That's because my blog is available in both English and
Brazilian Portuguese. To make this possible I translated the interface to
Portuguese and, more importantly, write each and every post in both English and
Portuguese.

The interface was really easy to translate, but needing to write each post
effectively twice is a lot of extra work. So why do I do it?

When I was thinking about creating the blog, I faced a dilemma: in which
language should I write in?

On one hand, writing in English would make my content widely available, since
it is the de facto language of the internet. It would make it possible for
example to share a post with a community I contributed to.

On the other hand, I like my mother language. And I'd want to be able to share
my posts with people I know personally without an artificial barrier: when both
me and the person are more familiar with Portuguese, it seems nonsensical to
transmit the information in English. I also didn't want to alienate people, from
my own country, which don't speak English (even though that is becoming ever
rarer).

So I decided to go with both 🙂. Double the work, but none of the downsides and
double the upsides. Actually it's not double the work, since the second post is
mostly a translation of the other one rather than a completely new post. The
fact that I don't post that often (once per month) also helps.

Open diary format
-----------------

Another thing is the way I approach my posts, and my motivation. Or the answer
to the question: There are a gazillion blogs out there already, why would anyone
read mine? Is it really worth it to have a blog?

And my answer to that is: Yes it is, because I'm not writing for others.

The main reason I write on my blog is because I want to record the projects I
did and stuff I found interesting, so that in the future I can look back to
those. So it's basically a diary (although monthly).

But of course if anyone is interested in any subject they're free to check the
posts, which makes this more of an open diary. That's very cool if it happens,
but I don't count on it. I write for myself, but if others like it that's a very
nice bonus 🙂.

This is why I don't tend to do tutorials, and instead document my experience
through learning or building something. It has a much more personal take, even
though it isn't as easily approachable by others.

(After writing down these two principles I do realize that they're a bit
contradictory, but what can I say, it's how I feel. I am human after all 😛.)

Since I was treating my blog as a diary it made sense to me to try and write
with a fixed frequency. I ended up deciding on once a month since that was
enough to keep a certain momentum while not as much as to make me stressed over
it.

I also ended up making the 20th of the month be my target date. This is only a
guideline. I don't force myself to do any of this. If I don't feel like posting
this month or can't make it to the 20th, that's okay. But by creating this
optional deadline there's a healthy pressure to make me win over any laziness
and think if there was anything interesting recently that I want to share (with
my future self and with others).

This has worked really well so far. I feel like I've always had something
interesting to share, and I did post every single month so far, missing just a
couple times the 20th (this one included 😝). By limiting myself to one post a
month I also manage to create a backlog of potential posts, even though there's
usually something left to do on each of them before they can be posted (I have
at least three like this at the moment).

Conclusion
==========

So, in summary, it's been a really nice journey so far. I'm happy to have
already accumulated 13 posts (14 with this one) I can look back at and be
nostalgic about stuff I built or learnt about, and maybe even get ideas for
future projects!

I'm not sure for how long this will keep going, but so far I don't see the end
🙂.

This was it about the "human" side of the blog. In the next post I'll talk about
the technical side to it: the customizations I did in the blog over this past
year to suit my needs.
