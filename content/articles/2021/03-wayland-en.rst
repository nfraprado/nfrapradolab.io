#################
Moving to Wayland
#################

:date: 2021-03-21
:tags: wayland


In the middle of January, my computer decided to surprise me, and not in a good
way. Differently from all the quirks I've come to expect from it after all these
6 years of use — faulty keyboard, flashing screen, bad audio jack — this time it
was worse, and it wasn't even a hardware issue.

At least once a day, my computer would completely freeze. The only action it
responded to was, yes, holding down the power button to force it to shutdown.
Not great.

I had the idea to try to SSH in using my phone (thank god for Termux) and that
worked. I listed the processes and saw ``xorg-server`` using 100% of CPU. I
killed it, and the computer magically came back to life. But obviously, having
just killed the graphical interface, all my open applications were gone with it,
and I was basically as good as getting back from a reboot anyway.

After about three days enduring this, trying to check logs to debug it, I was
done with X.Org.

I'm sure most of you know about it, and I'm certainly not well qualified to talk
about this, but X.Org, the application that has long powered the Linux graphical
interfaces is a giant mess. Its code is very hard to maintain, and its
architecture presents security concerns, to the point that a new alternative was
born quite a few years ago called Wayland_.

.. _Wayland: https://wayland.freedesktop.org/

The thing is, a lot of applications today still depend on X, which also means
moving to Wayland can be a breaking change. So although the move to Wayland for
the ecosystem as a whole seems inevitable, people are still confortable enough
with X.Org for the time being.

That was me as well, until X itself gave me a reason to quit. That issue could
probably be investigated and fixed, by why waste time on `something that will
soon enter maintenance mode`_? It was finally time I gave Wayland a shot.

.. _something that will soon enter maintenance mode: https://www.phoronix.com/scan.php?page=news_item&px=X.Org-Maintenance-Mode-Quickly

Making the switch
=================

From my point of view, switching to Wayland means essentially installing it and
changing all the applications I used on X, to equivalent ones that work on
Wayland. The main one being the graphical interface itself, which in my case was
the i3_ window manager.

.. _i3: https://i3wm.org/

Thankfully, there's an already very well established i3-compatible window
manager for Wayland called sway_, so that change was pretty straight-forward.
It was just a matter of installing sway and changing the config a bit.

.. _sway: https://swaywm.org/

Something that helped things a lot was that the sway wiki has a `i3 migration
guide`_, which not only shows the main changes needed in the i3 configuration
for it to work on sway, but also a list of the Wayland programs equivalent to
the ones normally used on X.Org.

.. _i3 migration guide: https://github.com/swaywm/sway/wiki/i3-Migration-Guide

For instance, the keyboard layout setup and toggling, which is normally handled
by setxkbmap in X, is integrated in sway so I just used ``input`` commands in
sway's config to configure the possible layouts and to have a binding for
toggling it.

Still on the keyboard topic, I have grown really accustomed to making my Caps
Lock work double duty: it acts as Esc when just pressed independently, and as
Control when held while pressing another key. I used setxkbmap together with
xcape to achieve this, but those obviously aren't an option on Wayland.

To my rescue came `Interception Tools`_, which allows the same to be done
regardless of being on X or Wayland. In itself it just provides the framework,
but the function of mapping both Esc and Control to Caps Lock I mentioned can be
easily configured by using the official plugin caps2esc_.

.. _Interception Tools: https://gitlab.com/interception/linux/tools
.. _caps2esc: https://gitlab.com/interception/linux/plugins/caps2esc

Oh, and it can also be combined with another plugin, space2meta_, to use the
Spacebar key as space when just pressed, but as the Meta key (the modifier on
i3/sway) when held. I had previously experimented with this in X using xcape but
I kept accidentally triggering bindings, so reverted back. But since space2meta
is smarter and only sends it on the key release event, it's really usable and
worth it for the increased comfort.

.. _space2meta: https://gitlab.com/interception/linux/plugins/space2meta

A bit of a side track: If caps2esc and space2meta sound like crazy talk to you,
then I do urge you to try them out. Mainly caps2esc, as that one doesn't have
any drawbacks, honestly. Like, how frequently do you use Caps Lock anyway?
There's no reason to keep it there in the home row. Esc and Control on the other
hand are really useful, even more if you use Vim. Now back on track...

What about rofi_, a menu launcher that I use for all sorts of stuff? It's also X
dependent. There's wofi_ as an alternative for Wayland, but I didn't really like
it. Felt a lot slower and I'm not a fan of it being GTK (the magnifying glass
icon beside the input field really bothers me). I just want a single color
rectangle with a prompt and options highlighted below, and rofi does that much
better. Luckily some brave soul created a `rofi fork for Wayland`_ and it works
like a charm. Mostly...

.. _rofi: https://github.com/davatorium/rofi
.. _wofi: https://hg.sr.ht/~scoopta/wofi
.. _rofi fork for Wayland: https://github.com/lbonn/rofi

Unfortunately, the window switching functionality doesn't work. And I find it
quite useful to go directly to a specific open application, otherwise I need to
cycle through all workspaces searching for it. But it was very easy to find a
window switcher script on the web, although for wofi, so I simply changed it a
bit to work with rofi and got myself `a window switcher script`_. And that's a
perfectly fine rofi in Wayland.

.. _a window switcher script: https://gist.github.com/nfraprado/484a0dfe60c49b7d2e9dfb45c3c4a4b5

Taking screenshots with maim was also no longer possible. The alternative is
grim_, and a separate program, slurp_, to be able to select a region or a window
for the screenshot. As a nice bonus, as grim's README shows, it's very simple to
make a color picker using it together with slurp and imagemagick, so I set that
up as well. My previous attempt at doing this with maim failed, so this was
a nice bonus of moving to Wayland.

.. _grim: https://github.com/emersion/grim
.. _slurp: https://github.com/emersion/slurp

xclip obviously also doesn't work on Wayland. So to keep my clipboard working, I
needed to install wl-clipboard_. This wasn't enough for Vim though. In Vim,
operating on the system clipboard is done through the ``+`` register, like
``"+yy`` to copy the current line to the clipboard, but that doesn't work with
Wayland's clipboard. I got really close to installing Neovim since it seems to
work there, but I found the vim-wayland-clipboard_ plugin for vim which works
flawlessly.

.. _wl-clipboard: https://github.com/bugaevc/wl-clipboard
.. _vim-wayland-clipboard: https://github.com/jasonccox/vim-wayland-clipboard

One thing I didn't expect to not work out of the box was screen sharing.
Thankfully `Pipewire's page on ArchWiki`_ gave instructions on how to get it
working, which was pretty simple too: just set an environment variable and
install xdg-desktop-portal-wlr_ and ``pipewire-media-session``. That said, I did
have an issue_ after a pipeWire update, but it seems to have been solved since.

.. _Pipewire's page on ArchWiki: https://wiki.archlinux.org/index.php/PipeWire#WebRTC_screen_sharing
.. _xdg-desktop-portal-wlr: https://github.com/emersion/xdg-desktop-portal-wlr
.. _issue: https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/768

I also changed my notification daemon from dunst to mako_. That said, as of
version 1.6.0, dunst supports Wayland as well although it still isn't packaged
on Arch Linux. But I'm fine with mako too to be honest, so will probably stay
with it.

.. _mako: https://github.com/emersion/mako

Some other changes that I don't think deserve any special comment:

* light_ instead of xbacklight to control the screen brightness
* gammastep_ instead of redshift to make the screen temperature more comfortable
  at night
* imv_ instead of sxiv as the image viewer

.. _light: https://github.com/haikarainen/light
.. _gammastep: https://gitlab.com/chinstrap/gammastep
.. _imv:  https://github.com/eXeC64/imv

And finally, it's important to mention Xwayland. It's the application that
allows you to run X applications on Wayland. It doesn't work for everything
(otherwise I wouldn't have changed all those previous applications), but for
example for Electron applications and for the Steam and games it has worked
flawlessly so far to that point that it's hard to know that they aren't native
on Wayland, which I think is awesome. If Wayland is to be the future, it has to
allow current X applications to continue to run on it until they are properly
ported over to Wayland.

Plot twist
==========

After all that change I was all set up on Wayland, but there were actually some
really annoying graphical glitches. From time to time, applications would start
flashing and glitching and there wasn't anything that made it stop, except, of
course, quitting Wayland. So I was essentially where I started, although a bit
better since in this case the whole computer didn't freeze, so I could at least
save everything before rebooting.

In the meantime though, I got a new computer from my employer, which was much
appreciated given all the quirks of the old one. Since I had already gone
through all the trouble of the Wayland transition, I installed sway in this new
computer as well. And everything worked perfectly. No issue whatsoever.

So, what is the conclusion of this story? The crashing/glitching issue was
something related to my old computer's hardware rather than X itself, but at
least that gave me the push I needed to migrate to Wayland. I could have
probably stayed on X on the new computer, yes, but that would only be delaying
the inevitable. The important thing is that now everything works, and I feel
much more comfortable debugging and reporting issues with the graphical
interface, given that it's actively maintained, if that ever happens in the
future.
