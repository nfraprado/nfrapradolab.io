##################################
Os blocos na minha barra de status
##################################

:date: 2021-11-26
:tags: i3blocks, desktop


Cinco anos atrás quando eu mudei para o gerenciador de janelas i3, eu comecei a
usar a barra de status dele, o i3bar. Ele é baseado em texto e depende de você o
que é mostrado lá. Mas ele não é muito modular: é estranho combinar informações
diferentes já que tudo precisa ser juntado em uma única string manualmente.

Mais ou menos um ano depois eu descobri um programa que resolve esse problema:
i3blocks_. O funcionamento dele é baseado em um config onde você define os
blocos que você quer e qual o script que vai ser rodado para cada um. A saída de
texto de cada script é o que vai ser mostrado para esse bloco na barra de
status.

.. _i3blocks: https://github.com/vivien/i3blocks

Meus blocos
===========

Eu atualmente tenho 9 blocos na minha barra de status: hora, rotina, tarefa,
bateria, teclado, armazenamento-root, armazenamento-home, atualização e música.

Hora
----

.. image:: {image}/time.png

Provavelmente o bloco mais óbvio. Eu não consigo pensar em nenhuma barra de
status que não mostre a hora atual...

Esse bloco mostra a data (dia da semana, dia, mês e ano) e hora. Mostrar a hora
em negrito é um detalhe que eu gosto bastante, mas não lembro de onde eu peguei
a ideia.

Rotina
------

.. image:: {image}/schedule.png

Esse bloco me mostra a rotina atual, ou seja, o que eu deveria estar fazendo
agora. Como você pode ver agora eu deveria estar lendo um livro ao invés de
escrever esse artigo para o blog... Mas eu preciso correr se quiser publicar
ele! 😝

Eu já mostrei esse bloco anteriormente no `<{article}org>`__.

Tarefa
------

.. image:: {image}/task.png

Esse bloco mostra o contexto de tarefas atual e o número de tarefas que eu tenho
na minha caixa de entrada, o número de projetos empacados e o número de tarefas
com prazos próximos.

Quando todos os três são zero, esse bloco é escondido, apesar de isso ser bem
raro. Como você pode ver, eu não tenho tido tempo ultimamente para organizar as
tarefas da minha caixa de entrada 😅.

Eu também já mostrei esse bloco no artigo `<{article}org>`__.

Bateria
-------

.. image:: {image}/battery.png

Esse bloco mostra a porcentagem atual de carga da bateria. O ícone reflete a
carga atual (dentre 4 possibilidades), e quando a bateria está baixa o fundo fica
vermelho para chamar minha atenção.

Quando a bateria está carregando e com uma carga alta, esse bloco fica oculto.

Teclado
-------

.. image:: {image}/keyboard.png

Esse bloco me mostra o layout atual do teclado. Já que eu só uso dois layouts,
pt-br (ABNT2) e en-us, e na maior parte do tempo eu uso o inglês, esse bloco só
mostra em vermelho quando eu estou no layout português. Quando estou no inglês,
ele fica oculto.

Eu adicionei esse bloco depois de tentar usar o vim várias vezes tendo esquecido
que estava com o layout português.

Armazenamento
-------------

.. image:: {image}/storage-root.png

.. image:: {image}/storage-home.png

Esses dois blocos mostram o espaço de armazenamento disponível e total nos meus
discos. O primeiro (ícone de computador) mostra para a partição root, enquanto o
segundo (ícone de casa) mostra para a partição home. Esses blocos ficam quase
sempre escondidos, a não ser que o espaço disponível fique baixo. Nesse caso
eles aparecem com o fundo vermelho.

**Obs**: O espaço livre mostrado nessas fotos normalmente não seria considerado
baixo, eu que forcei eles a aparecerem.

Atualização
-----------

.. image:: {image}/update.png

Esse bloco me mostra o número de pacotes que precisam ser atualizados. Ele
apenas aparece se o número for alto o suficiente, caso contrário fica oculto.

Antigamente eu fazia com o que o fundo desse bloco ficasse vermelho se o pacote
do kernel precisasse ser atualizado, já que eu percebi que no Arch Linux
atualizar o kernel não mantém os módulos da versão anterior. Isso significa que
sempre após atualizar o kernel o sistema deve ser reiniciado, caso contrário
alguns bugs estranhos podem acontecer devido à falta de módulos necessários (por
exemplo, USBs não funcionarem). Porém depois de descobrir o pacote
kernel-modules-hook_ que resolve esse problema, eu removi essa checagem e nem me
preocupo mais na hora de atualizar o kernel.

.. _kernel-modules-hook: https://archlinux.org/packages/community/any/kernel-modules-hook/

Música
------

.. image:: {image}/music.png

Esse bloco me mostra a música que está sendo tocada atualmente. Ele mostra o
artista seguido do nome da música, e o ícone reflete o estado atual: tocando ou
pausado. Quando nenhuma música está tocando ele fica oculto.

Como nome de banda ou música podem ficar grandes, a partir de um certo tamanho
eu omito o restante usando um "...".

Bloco antigo: cardapio-unicamp
------------------------------

.. image:: {image}/cardapio-unicamp.png

.. image:: {image}/cardapio-unicamp-semana.png

Esse não é um bloco que eu uso atualmente, mas era meu favorito então eu quero
mencioná-lo. Seu propósito era me informar qual o cardápio do bandejão da
faculdade, para que eu pudesse decidir se queria comer lá ou em outro lugar.

Houveram duas iterações. No começo, eu simplesmente mostrava a descrição do
prato principal da próxima refeição. Depois de um tempo eu mudei ele para me
mostrar a qualidade das refeições para a semana inteira. Eu fiz isso comparando
a descrição do prato com palavras que eu considerava bom ou ruim. O resultado
final eram dez quadrados, cada dia separado por um ``|`` e em cada um o primeiro
quadrado mostrava almoço e o segundo, a janta. A cor dos quadrados mostrava a
qualidade: branco para normal, verde para bom e vermelho (ou magenta nesse
esquema de cores antigo) para ruim.

Os cardápios eram obtidos usando um programa em python que eu escrevi chamado
cardapio-unicamp_. O script para obter a qualidade dos pratos para a semana
inteira `também está disponível nesse repositório`__. A conversão de ``|``,
``+`` e ``-`` para os quadrados coloridos eu não tenho publicada, mas eram
algumas simples (mas feias) linhas de bash, então eu tenho certeza que você
consegue fazer algo melhor que isso se quiser 🙂.

.. _cardapio-unicamp: https://gitlab.com/nfraprado/cardapio-unicamp/
.. __: https://gitlab.com/nfraprado/cardapio-unicamp/-/blob/master/exemplos/cardapio_ru_semana.py

Melhorias
=========

Eu realmente gosto da configuração atual da minha barra de status, mas levou
tempo e melhorias para chegar aqui.

Por exemplo eu sinto que consistência é importante para que seja fácil de
identificar cada bloco: cada um tem sua própria cor acima e um pouco de espaço
em volta, e seu ícone sempre é a primeira coisa à esquerda, seguido do texto.

Eu também aproveito o uso de cores para reduzir a quantidade de texto usada nos
blocos. Por exemplo, o bloco de tarefa possui três números diferentes, mas eles
não precisam de legenda, já que cada cor já deixa evidente seu significado.

Reduzir a quantidade de coisa na barra de status me ajudou a focar no que é
realmente relevante no momento. Para isso eu comecei a me questionar quais
informações realmente eram importantes. Eu não me importo em qual rede wifi eu
estou conectado na maior parte do tempo, mas eu tinha um bloco para isso (e é um
bem comum), então eu só removi ele. E tem informações que só são relevantes
às vezes, e é por isso que agora meus blocos ficam ocultos a não ser que a
informação atual deles seja relevante.

Eu também reduzi o número de atualizações inúteis dos blocos fazendo com que
sempre que possível eles atualizassem com base em sinais ao invés de tempo.
Então por exemplo o bloco de teclado apenas atualiza quando eu aperto o atalho
de mudança do teclado, que manda um sinal para esse bloco.

Ligar cada bloco a um sinal também faz com que os blocos atualizem mais rápidos
para eventos assíncronos. Por exemplo, eu atualizo os pacotes no meu computador
usando um comando, que além de atualizar usando o pacman, manda um sinal para
o bloco de atualização no final. Então assim que eu acabo de atualizar os
pacotes, esse bloco desaparece, e não depois de um certo tempo.

Com o passar do tempo eu também escrevi um pouco de código para deixar as
definições de blocos mais fáceis. Como outras partes do meu sistema, eu comecei
meus blocos em bash, mas eventualmente migrei eles para python. Como parte disso
eu escrevi uma função ``print_i3blocks`` em python que recebe o ícone, texto e
cor para o bloco e escreve eles na saída no formato JSON necessário para o
i3blocks.

Eu também tenho um comando ``update_i3blocks``  que recebe o nome do bloco e
envia o sinal correspondente, assim eu posso atualizar um bloco específico a
partir de outros comandos no sistema.

O bug irritante
===============

Quando eu migrei os scripts dos blocos para python eu comecei a ter problemas
com os blocos desaparecendo aleatoriamente. Levou quase um ano para realmente
entrar no código do i3blocks e encontrar e `consertar esse problema`__. Apesar
de ter sido difícil de achar, foi muito interessante entender esse problema. Não
tinha nada a ver com o python, é só que como código em python roda mais devagar
que bash, ele ficou mais aparente.

.. __:  https://github.com/vivien/i3blocks/pull/454 

Infelizmente, já que o repositório do i3blocks não está mais ativamente mantido,
se você estiver tendo esse mesmo problema e quiser consertá-lo, vai precisar
aplicar o patch e compilar por si mesmo (apesar de ser bem simples). De qualquer
forma eu não consigo nem dizer o quão feliz estou de finalmente ter me livrado
desse bug. Agora eu posso simplesmente relaxar e aproveitar minha barra de
status mostrando todas, e apenas, as informações que são relevantes para mim 🙂.
