#######################################
Revivendo um computador no meio do nada
#######################################

:date: 2021-10-23
:tags: android, linux


No começo desse mês, eu estava passando umas semanas em outro país. Era tarde e
eu estava mais uma vez olhando meus arquivos pessoais e pensando se havia uma
forma melhor de organizá-los em pastas.

Depois de pensar um pouco, eu decidi usar uma nova estrutura e movi alguns
arquivos. No meio tempo, eu percebi que eu não tinha atualizado meus pacotes já
há algum tempo, então eu comecei uma atualização.

No final da atualização eu vi um erro. O ``/etc/mkinitcpio.conf`` não tinha sido
encontrado. Meu ``/etc/mkinitcpio.conf`` é um link simbólico para um arquivo na
minha home já que isso torna mais fácil de acompanhar as mudanças nesse config
como parte dos meus backups. E entre os arquivos que eu tinha acabado de mover
estava justamente esse config, então eu atualizei o link para apontar para a
nova localização. Eu rodei a atualização de novo e o ``pacman`` reportou
"nothing to be done". Eu inocentemente acreditei que estava tudo certo agora.

Então eu reiniciei para checar que estava tudo funcionando com os arquivos no
novo lugar. Assim que eu selecionei meu sistema no GRUB, ele reportou que o
``/initramfs-linux.img`` estava faltando. Mesma coisa para o initramfs de
reserva (*fallback*). Eu estava oficialmente trancado para fora do meu sistema.

Agora tinha ficado claro o que aconteceu. Durante a atualização o kernel foi
atualizado, e como parte disso a initramfs foi re-gerada, mas já que o arquivo
de configuração estava inválido, isso foi abortado. Quando eu rodei o comando de
atualização novamente, o ``pacman`` concluiu que nada mais precisava ser
atualizado e assim disse. O que eu deveria ter feito é rodar manualmente o
comando de instalação do pacote do kernel novamente para que a initramfs fosse
de fato gerada. Como eu não fiz isso, não havia mais nenhum arquivo de initramfs
na minha partição de boot, e portanto eu não conseguia inicializar o sistema.

Eu respirei fundo e comecei a pensar o que eu podia fazer. Eu só precisava de um
pendrive com a ISO do ArchLinux para bootar dele e rodar de novo a instalação do
pacote de kernel para arrumar essa bagunça. Mas já que eu estava em outro país,
meus equipamentos eram limitados. Felizmente eu tinha um pendrive, mas não tinha
outro computador para gravar a ISO.

A não ser... A não ser que eu pudesse usado meu amado celular Nexus 5X para
gravar. A porta de carregamento dele é USB tipo C, e eu suspeitava que ela
também funcionasse como OTG. Então eu tinha um pendrive e um cabo para conversão
de USB A para C, só restava descobrir se era de fato possível gravar uma ISO do
celular.

Eu imediatamente abri a F-Droid e pesquisei por um gravador de ISO. Para minha
felicidade encontrei o EtchDroid_. Sua descrição realmente ressoou comigo:
"Você pode usá-lo para criar um pendrive bootável com GNU/Linux quando seu
laptop está morto e você está no meio do nada." (tradução minha). Percebendo que
talvez realmente fosse possível, eu fui na página do Arch Linux e baixei a ISO.

.. _EtchDroid: https://f-droid.org/en/packages/eu.depau.etchdroid/

Eu então instalei o EtchDroid e abri ele. Eu selecionei a opção de gravar uma
ISO, selecionei o arquivo da ISO, conectei o pendrive no celular e selecionei
ele no aplicativo. Eu realmente gostei da interface simples dele:

.. image:: {image}/etchdroid.png

Eu apertei o botão de iniciar a gravação e uma notificação apareceu mostrando o
progresso:

.. image:: {image}/flash_in_progress.png

E depois de alguns segundos, tinha acabado:

.. image:: {image}/flash_done.png

Eu então desconectei o pendrive do celular e conectei no meu notebook. Eu fiquei
muito feliz de ver que a imagem USB tinha funcionado e bootei dela.

Para consertar o problema eu configurei meus *mountpoints*, fiz um *chroot* para
dentro do sistema, e rodei ``pacman -S linux`` para reinstalar o pacote do
kernel e a initramfs ser re-gerada.

Com a initramfs de volta, eu reiniciei e tudo funcionou perfeitamente de novo
🙂.

Eu estou muito feliz que apesar do meu erro besta, eu consegui resolvê-lo bem
rápido mesmo em um ambiente limitado graças a esse excelente aplicativo chamado
EtchDroid. Com certeza vou guardar ele para futuras emergências.
