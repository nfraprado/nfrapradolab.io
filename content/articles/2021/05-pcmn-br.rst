########################
Gerenciando meus pacotes
########################

:date: 2021-05-20
:tags: sysadmin, python


Como eu mencionei anteriormente no `<{article}wayland>`__, eu recentemente
mudei de computador. Mudanças podem ser bem trabalhosas se você usa um sistema
muito customizado e não tem todas as configurações facilmente disponíveis para
fazer a mudança. Já que eu faço backup dos meus arquivos, que inclui uma pasta
``dotfiles`` com todos (ou quase todos) os arquivos de configuração para os
programas que eu uso, eu pensei que seria bem simples.

Mas durante a mudança, eu percebi que eu não tinha um jeito fácil de ver e
instalar todos os programas que eu uso. Nesse momento eu comecei a pensar em
usar uma distro como o NixOS_ que usa um gerenciador de pacotes declarativo
justamente para tornar fácil reproduzir um sistema. Mas eu gosto bastante do
`Arch Linux`_, e sinceramente não existe nenhuma razão para eu não poder ter
essa funcionalidade nele.

.. _NixOS: https://nixos.org/
.. _Arch Linux: https://archlinux.org/

Gerenciamento de pacotes declarativo
====================================

O objetivo então era ter um gerenciamento de pacotes declarativo para tornar
mudanças futuras mais fáceis. Inclusive, como nesse método os pacotes são
listados em um arquivo, com o versionamento dele nos meus backups eu também
ganho de brinde a possibilidade de voltar para um versão anterior (se eu quiser
reverter algum erro na lista de pacotes, por exemplo).

Mas ter uma simples lista dos pacotes instalados não é a melhor coisa sobre o
gerenciamento de pacotes declarativo. O maior benefício é que você pode
*organizar* essa lista de uma forma que faça sentido para você, reordenando e
agrupando os pacotes conforme necessário. E você pode deixar comentários pelo
arquivo inteiro, que é útil principalmente para registrar a razão de ter
instalado cada pacote, assim você pode usar essas informações para remover os
pacotes que não são mais necessários.

Uma busca rápida na internet por gerenciadores de pacotes declarativos para o
Arch Linux revelaram o aconfmgr_ e o decpac_. O primeiro é mais completo,
contendo também gerenciamento de configurações integrado, mas isso já é demais
para mim, e ele é em bash, então ponto negativo. O segundo é bem simples, você
basicamente tem apenas um arquivo com os comandos para instalar os pacotes e os
pacotes em si em uma sintaxe parecida com JSON. E é em python.

.. _decpac: https://github.com/rendaw/decpac
.. _aconfmgr: https://github.com/CyberShadow/aconfmgr

O decpac também permite instalar do AUR_ através do uso da ferramenta yay_.
Os pacotes do AUR são identificados por uma marcação simples antes do nome do
pacote.

.. _AUR: https://aur.archlinux.org/
.. _yay: https://github.com/Jguer/yay

O decpac é *quase* perfeito, mas eu não gostei que ele usa o mesmo arquivo tanto
para a configuração dos comandos quanto para a lista de pacotes, porque isso faz
com que a sintaxe precise ser mais complexa, e ela é similar a JSON mas não
exatamente.

E foi por isso que eu decidi criar o meu próprio, que eu chamei, bem
criativamente, de pcmn.

pcmn
====

O pcmn_ é bem simples, com menos de 200 linhas de código em python. Ele é
bastante inspirado no decpac com a principal diferença sendo a sintaxe mais
simples. E é bem simples mesmo:

* um pacote por linha (mas linhas sem nenhum pacote são permitidas também, claro)

* tudo depois de um ``#`` é um comentário

* opcionalmente o grupo do pacote pode ser escrito entre colchetes antes do nome
  do pacote. Quando nenhum grupo é especificado, o padrão é assumido, que
  instala do repositório oficial. O único outro grupo é ``aur``, que informa que
  o pacote deve ser instalado do AUR usando o yay.

.. _pcmn: https://gitlab.com/nfraprado/pcmn

Só existem dois comandos:

* ``generate`` cria uma nova lista de pacotes com base nos pacotes atualmente
  explicitamente instalados (ou seja, as dependências não vão aparecer na lista,
  mas não é problema porque o pacman vai resolvê-las).

* ``apply`` aplica a lista de pacotes, ou seja, pacotes não listados são
  desinstalados e pacotes na lista que não estiverem presentes são instalados.

Um arquivo de configuração separado em JSON pode ser usado para mudar os
comandos usados para listar, instalar e remover os pacotes, tanto para o grupo
padrão quanto para o ``aur``.

Eu estou usando ele faz alguns meses já e estou bem satisfeito. Como eu
mencionei, a principal diferença do decpac é que eu separei a configuração e a
lista de pacotes e eu acho que isso fez toda a diferença. A lista de pacotes é
bem limpa e fácil de adicionar itens. Esse é um pedaço da minha:

.. include:: {code}/pkglist
   :code: python

Toda vez que eu percebo que preciso de algum pacote a mais para alguma coisa eu
adiciono na lista, coloco um comentário sobre o motivo, e rodo ``pcmn apply``
para ele ser instalado. Quando vou instalar um pacote temporariamente só para
fazer algo bem rápido ou algum teste, eu instalo manualmente com o pacman mesmo,
assim da próxima vez que fizer um ``apply`` ele já é desinstalado.

Se você achou o pcmn interessante, dê uma olhada `no repositório dele <pcmn_>`__
para um pouco mais de informação. Ele também é facilmente instalável `do AUR`__,
o que significa que depois de você fazer gerar a lista de pacotes com
``generate``, ``[aur] pcmn-git`` vai aparecer na lista de pacotes que ele mesmo
gerencia. Eu acho isso engraçado por algum motivo 😝.

.. __: https://aur.archlinux.org/packages/pcmn-git/

E é por isso que eu gosto tanto de python. Com menos de 200 linhas de código eu
escrevi um programa que resolve um problema real que eu estava tendo. `E também
foi divertido de escrever!`__

.. __: https://xkcd.com/353/
