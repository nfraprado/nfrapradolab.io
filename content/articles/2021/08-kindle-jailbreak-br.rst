##################
Domando meu Kindle
##################

:date: 2021-08-29
:tags: kindle


Vários anos atrás minha mãe me deu um Kindle Paperwhite 2. Eu li alguns
livros nele desde então, mas eu nunca senti que ele realmente era meu. Bloquear
a tela mostrava um anúncio, todos os livros comprados da Amazon eram protegidos
por DRM, livros transferidos via USB só funcionavam se estivessem no formato
.mobi da Amazon, e eu sempre me sentia desconfortável sabendo que cada página
que eu virasse podia estar sendo reportada de volta para a central deles.

Recentemente eu decidi dar um basta nisso e descobrir como fazer jailbreak no
meu Kindle. Eu estava disposto a torná-lo meu ou perdê-lo no processo.

Fazendo o Jailbreak
===================

Depois de pesquisar um pouco achei `um artigo muito bem mantido em um fórum
sobre como fazer jailbreak no Kindle`__. Na verdade esse fórum 
mobileread.com inteiro parece ser o ponto focal de informações sobre fazer
jailbreak e customizações em dispositivos de leitura de e-book. Eu acho incrível
como existem essas bolhas na internet cheias de informações específicas e que
você nunca encontra a não ser que procure por elas.

.. __: https://www.mobileread.com/forums/showthread.php?t=320564

Eu comecei a seguir o guia, que é na verdade bem simples: o jailbreak em si
consiste em conectar o Kindle no computador pelo USB, adicionar um arquivo nele
e fazer o arquivo rodar. Mas nesse momento eu percebi um grande problema que eu
já suspeitava: o USB no meu Kindle não funcionava mais.

A porta micro USB no Kindle serve tanto para transferência de energia, para
carregar o dispositivo, quanto transferência de dados, para ser possível montar
ele no computador como um dispositivo de armazenamento de dados e transferir
arquivos. Mas de alguma forma a transmissão de dados não funcionava mais, então
eu não conseguia enviar arquivos para o Kindle. Pelo menos a energia ainda
funcionava, então eu ainda conseguia recarregar ele.

Com o principal canal de dados para o Kindle quebrado, eu percebi que minhas
alternativas eram ou descobrir o problema no USB, que eu imaginava que
seria problema no hardware do controlador USB e precisaria trocar o CI por um
novo, ou checar se o Kindle tinha uma interface UART funcional que eu poderia
usar no lugar do USB. Essa segunda opção parecia bem mais fácil se fosse
possível.

Fazendo o Jailbreak pela UART
-----------------------------

De fato era possível acessar a UART do Kindle PW2 como `esse artigo <artigo
sobre o jailbreak pelo serial_>`__ mostra. Ele também mostra como abrir o Kindle
e dá algumas ideias de como tornar o acesso ao UART permanente.

.. _artigo sobre o jailbreak pelo serial: https://www.mobileread.com/forums/showthread.php?t=267541

A única parte difícil em abrir foi o primeiro passo: fazer a tampa da frente
desgrudar do resto. Tendo parado de tocar violão alguns anos atrás, eu nunca
pensei que esse seria o jeito que uma palheta seria útil para mim de novo 😝:

.. image:: {image}/open-pick.jpg

Com a ajuda da palheta eu desgrudei a tampa e depois de desparafusar alguns
parafusos eu cheguei nas entranhas (a segunda foto mostra os parafusos da PCB
também desparafusados e cabos flat desconectados já que eu estava fuçando, mas
não tem nada de interessante debaixo da PCB):

.. image:: {image}/open-nocover.jpg

.. image:: {image}/open-board.jpg

Nessa etapa eu já estava pronto para testar se a UART realmente funcionava. Um
problema é que a UART do Kindle usa 1.8V e meu cabo UART é 3.3V. Para contornar
esse problema eu usei um divisor de tensão para abaixar a tensão para o RX do
Kindle, e dois transistores bipolares e um regulador de tensão de 3.3V para
aumentar a tensão do TX do Kindle (tenho que admitir que eu pesquisei na
internet por esse circuito, mas pelo menos depois que eu vi eu fui capaz de
verificar como funcionava e vou lembrar dele para projetos futuros 🙂). Essa foi
a configuração temporária:

.. image:: {image}/temp-mod-uart.jpg

Com isso eu rodei o ``picocom`` na porta serial e reiniciei o Kindle. Essa foi a
saída que eu obtive (só pedaços para não ficar muito longo):

.. include:: {code}/boot_log
   :code:

Opa, oi Linux 3.0, há quanto tempo você está aí? 🙂

Enviar comandos pelo serial também estava funcionando, então eu estava pronto
para seguir com o jailbreak. Eu continuei seguindo o `artigo sobre o jailbreak
pelo serial`_ até que consegui root, mas para minha surpresa depois disso ele
mostra como transferir o arquivo do jailbreak pelo armazenamento USB, que eu não
posso usar. Mas já que consegui fazer o serial funcionar, eu não via porque não
poderia usá-lo para transferir o arquivo, apesar de nunca ter feito isso antes.

Mas claro que isso é possível, e descobri que é feito usando os protocolos de
transferência de arquivos xmodem, ymodem ou modem. Todos eles são providos pelo
pacote lrzsz_ no Arch Linux. Já que o Kindle apenas possuía o binário do ``rx``,
que é o receptor do xmodem, eu precisava usar o ``sx`` no meu computador, para
enviar usando o protocolo xmodem também.

.. _lrzsz: https://archlinux.org/packages/community/x86_64/lrzsz/

O procedimento para enviar um arquivo pelo serial usando o xmodem é:

.. _procedimento do xmodem:

* rodar ``picocom`` passando ``--send-cmd 'sx'``.
* no serial (o shell do Kindle no meu caso), digitar ``rx <filename>`` onde
  ``filename`` é o nome que vai ser dado para o arquivo recebido no sistema de
  arquivo destino.
* apertar ``Ctrl+a`` seguido de ``Ctrl+s``. Isso vai instruir o picocom a enviar
  o arquivo, usando o ``sx`` para a transferência já que foi ele que foi passado
  como o ``--send-cmd`` na hora de rodar o picocom.
* no prompt que aparece, digitar o nome do arquivo que vai ser transferido da
  máquina origem, relativo ao diretório que o picocom foi executado.
* esperar a transferência terminar (pode levar um tempo).

Vale ressaltar um detalhe importante em transferir arquivos dessa forma e que eu
levei um tempo para descobrir, e que talvez seja específico da minha conexão com
o Kindle, não tenho certeza, mas é que **a transferência só vai funcionar se o
Kindle estiver carregando**. Por algum motivo, parece que quando ele não está
carregando os sinais TX/RX não são estáveis o suficiente a ponto dos *checksum*
que são enviados durante a transferência do xmodem não serem válidos e a
transferência falha. Mas com o carregador conectado e seguindo os passos
descritos acima, eu não tive nenhum problema em transferir arquivos. Usar o
shell, por outro lado, funciona suficientemente bem mesmo sem ter o carregador
conectado.

A única coisa é que a transferência é demorada. A velocidade de transferência é
aproximadamente 10 KB/s, então o arquivo de jailbreak (~160 KB) levou 15
segundos para transferir, já arquivos grandes como o pacote do KOReader (~37
MB), que vou mencionar logo abaixo, levou por volta de uma hora. Pelo USB o
primeiro teria sido instantâneo, e o segundo teria levado alguns segundos. Isso
me fez perceber o quanto evoluímos em velocidade de transferência, então esse
método tem seu próprio charme 😝.

Com essa configuração eu pude transferir o arquivo, seguir o restante do guia e
completar o jailbreak do meu Kindle com sucesso! Eu também instalei o KUAL e o
MRInstaller como sugerido nos guias para que pacotes sejam fáceis de instalar e
possam ser acessados por uma GUI no Kindle.

Tornando ele meu
================

Agora que eu tinha um Kindle com jailbreak era hora de finalmente torná-lo meu.
A necessidade de usar o formato mobi é irritante, eu preferiria usar um formato
mais padrão como o epub. Felizmente isso é possível em um Kindle com jailbreak
usando a aplicação KOReader_.

KOReader
--------

O KOReader_ é um leitor de e-book que roda em Kindles com jailbreak (e também
outros dispositivos) e suporta múltiplos formatos de e-book como epub e pdf, que
é exatamente o que eu queria.

.. _KOReader: https://github.com/koreader/koreader

É sinceramente um ótimo programa: Ele é muito ativamente mantido e melhorado, e
tem todas as funcionalidades que eu estava acostumado no leitor padrão do Kindle
como marcar páginas, pesquisar no dicionário, pesquisar no texto e navegar pelos
capítulos, além de muitas outras e diversas configurações dentro dos seus vários
menus.

Minhas funcionalidades preferidas, que são melhorias em relação ao leitor padrão
do Kindle, são a barra de progresso na parte inferior da tela mostrando sua
localização atual no livro, e o fato de que quando o dispositivo é bloqueado, a
capa do livro é mostrada na tela! 😃 Isso me deixou muito feliz de ver. Sem mais
anúncios ou imagens aleatórias da Amazon, só a capa do livro que eu estou lendo.
Na verdade, eu originalmente tinha instalado o ScreenSavers Hack para ser
possível colocar um bloqueio de tela logo depois que fiz o jailbreak no Kindle,
mas depois que percebi que o KOReader já tinha essa funcionalidade embutida eu
removi esse pacote.

Minha única queixa com o KOReader é que às vezes ele fica bem lento, e então eu
preciso reiniciá-lo (que leva só alguns segundos) e ele volta à velocidade
normal. Mas eu provavelmente deveria atualizá-lo já que eles liberam uma nova
versão todo mês e já fazem quatro meses desde que instalei, então esse problema
pode já ter sido resolvido. (Sim, tudo descrito nesse artigo aconteceu por volta
de abril)

Conector UART
-------------

A fim de tornar meu Kindle à prova de vigilância eu precisava manter o WiFi
sempre desligado (também para manter o jailbreak funcionando). E já que
transferir via USB não funciona mais, o UART seria minha única interface com o
Kindle. Isso significa que eu preciso conseguir transferir os livros para o
Kindle através do UART com certa facilidade. Eu não considero precisar abri-lo
fácil o suficiente então eu precisava de um acesso ao UART mais definitivo.

A ideia era basicamente fazer os fios conectados aos pontos de UART na PCB
ficarem mais fixos, abrir um buraco na lateral da tampa de plástico do Kindle e
expor alguns pinos ali para que fosse fácil conectar um cabo UART no Kindle sem
precisar abri-lo.

Um problema que já mencionei é que a tensão de operação do meu cabo UART
é diferente da do Kindle, o que foi o motivo para eu ter feito o circuito
conversor na protoboard. Eu inicialmente pensei em colocar o circuito conversor
dentro do próprio Kindle, mas percebi que eu não tinha componentes pequenos o
suficiente para que coubesse ali. Então no fim acabei aceitando que seria
necessário um circuito conversor externo.

A abertura na tampa para expor os pinos de serial foi feita pelo meu pai! 😃

.. image:: {image}/mod-case.jpg

.. image:: {image}/mod-case2.jpg

.. image:: {image}/mod-case3.jpg

A solda dos fios nos pontos de UART da PCB foi um pouco complicada já que os
pontos são bem pequenos. Mas com um pouco de ajuda de fita isolante para
segurar o fio enquanto eu soldava e escolhendo um fio adequado (aqueles usados
dentro de cabos Ethernet funcionaram bem) eu dei um jeito.

Ainda sim eu estava preocupado em esbarrar no fio sem querer e desconectar ele,
ou pior ainda, causar algum dano irreparável no ponto de UART, então eu acabei
grudando os fios com fita isolante na placa a cada alguns centímetros, como
*checkpoints*, e funcionou super bem. Eu vou certamente continuar usando essa
técnica no futuro.

.. image:: {image}/mod-uart-connection.jpg

Eu deixei um pouco de fio sobrando caso seja necessário no futuro.

.. image:: {image}/mod-uart-wires.jpg

Fazer o fio ficar bem conectado nos pinos do conector foi complicado também. Eu
torci cada fio em volta de cada pino, mas não tão apertado para que o fio não
quebrasse (isso aconteceu algumas vezes), e adicionei um pouco de solda. Depois
disso eu coloquei fita isolante em volta de cada pino para que os fios de cada
pino não tocassem um ao outro. Por fim adicionei mais fita isolante em volta de
todos os pinos para isolar eles do corpo cinzento do Kindle, já que ele é um
material condutor.

.. image:: {image}/mod-uart-header.jpg

Depois de centralizar razoavelmente os pinos no buraco, eu coloquei a durepoxi_
e moldei ela em volta dos pinos, em cima e embaixo, para que ficassem bem fixos
no corpo do Kindle.

.. _durepoxi: https://www.loctite-consumo.com.br/pt/produtos/durepoxi.html

.. image:: {image}/mod-uart-epoxy.jpg

Depois de deixar secar durante a noite, a cirurgia estava completa:

.. image:: {image}/mod-final.jpg

Ah, e eu fiz tudo isso duas vezes. A primeira vez eu cheguei até o fim e só
então percebi que dois dos pinos estavam curto circuitados pelo corpo cinzento
do Kindle. Felizmente a durepoxi não estava totalmente rígida ainda então eu
consegui removê-la com um pouco de perseverança e tentar de novo.

Resultado
=========

Então agora eu tenho um Kindle com os pinos UART expostos, e foi assim que
ficou:

.. image:: {image}/result-pin.jpg

.. image:: {image}/result-front.jpg

Agora quando eu quero ler um novo livro, eu conecto o carregador e a placa
conversora nele (eu ainda estou usando a protoboard para o circuito conversor
porque ainda não superei a preguiça para fazer uma placa decente, mas não seria
difícil e seria bem mais compacta):

.. image:: {image}/result-transfer-setup.jpg

Aí eu sigo o `procedimento do xmodem`_ que mencionei acima para enviar o arquivo
do livro pelo UART e espero um pouco até a transferência acabar...

.. include:: {code}/xmodem
   :code:

Esse livro levou 3 minutos para transferir. O tamanho padrão de livros epub é
entre 100 KB e 10 MB, então a transferência leva entre 10 segundos e 20 minutos
dependendo do livro. É significantemente mais tempo do que levaria por USB
(alguns segundos) mas já que eu levo entre um e dois meses para acabar de ler
cada livro acaba não sendo um problema.

Quando a transferência termina, tudo que resta é desconectar os cabos e
aproveitar a leitura! 🙂

.. image:: {image}/result-book.jpg

(Eu uso essa capa azul no Kindle para protegê-lo, e os pinos são curtos o
suficiente que eles cabem embaixo também)

Obs: Esse livro está disponível em domínio público e pode ser `baixado de graça
no Project Gutenberg`__.

.. __: https://www.gutenberg.org/ebooks/48320

Conclusão
=========

Deu um pouco mais de trabalho do que eu esperava inicialmente, mas depois disso
tudo eu estou feliz que finalmente realmente sou o dono do meu Kindle e posso
ter uma experiência de leitura melhor nele 🙂.
