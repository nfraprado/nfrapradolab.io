PY?=python3
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/public
CONFFILE=$(BASEDIR)/cfg_main.py
PUBLISHCONF=$(BASEDIR)/cfg_publish.py
STATSCONF=$(BASEDIR)/cfg_stats.py
THEMEDIR=$(BASEDIR)/theme


DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICANOPTS += --relative-urls
endif

help:
	@echo 'Makefile for a pelican Web site                                                 '
	@echo '                                                                                '
	@echo 'Usage:                                                                          '
	@echo '   make html                           (re)generate the web site                '
	@echo '   make clean                          remove the generated files               '
	@echo '   make regenerate                     regenerate files upon modification       '
	@echo '   make publish                        generate using production settings       '
	@echo '   make serve [PORT=8000]              serve site at http://localhost:8000      '
	@echo '   make serve-global [SERVER=0.0.0.0]  serve (as root) to $(SERVER):80          '
	@echo '   make devserver [PORT=8000]          serve and regenerate together            '
	@echo '   make trans_update                   update translation files from templates  '
	@echo '   make trans_deploy                   apply translation files                  '
	@echo '                                                                                '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html         '
	@echo 'Set the RELATIVE variable to 1 to enable relative urls                          '
	@echo '                                                                                '

html:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean:
	rm -rf $(OUTPUTDIR)/*

regenerate:
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
ifdef PORT
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

serve-global:
ifdef SERVER
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b $(SERVER)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b 0.0.0.0
endif


devserver:
ifdef PORT
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

publish:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

trans_update:
	cd $(THEMEDIR) && \
	pybabel extract --mapping babel.cfg --output messages.pot ./ && \
	pybabel update --input-file messages.pot --output-dir translations/ --domain messages

trans_deploy:
	cd $(THEMEDIR) && \
	pybabel compile --directory translations/ --domain messages

stats:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(STATSCONF) $(PELICANOPTS)

.PHONY: html help clean regenerate serve serve-global devserver publish trans_update trans_deploy stats
