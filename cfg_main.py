#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "Nícolas F. R. A. Prado"
SITENAME = "nfraprado"
SITEURL = ""

PATH = "content"
OUTPUT_PATH = "public/"

TIMEZONE = "America/Sao_Paulo"

DEFAULT_LANG = "en"

THEME = "theme"

# Desired feeds are set in publish config. Avoids feed generation locally.
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

STATIC_PATHS = ["images", "code"]

EXTRA_PATH_METADATA = {
    "images/favicon.ico": {"path": "favicon.ico"},
}

# Before the dash, use to match translations. After dash assign to language.
FILENAME_METADATA = "(\d\d-)?(?P<id>.*)-(?P<lang>br|en)"
ARTICLE_TRANSLATION_ID = "id"

PLUGIN_PATHS = ["plugins"]
PLUGINS = [
    "i18n_subsites",
    "series",
    "rst_no_tt_reader",
    "linker.image",
    "linker.article",
]

# Directory for code relative to the PATH variable to be used for {code} in
# '.. include::'
CODE_DIR = "code"

I18N_SUBSITES = {
    "br": {
        "STATIC_PATHS": ["images"],
    }
}

DEFAULT_DATE_FORMAT = "%b %d, %Y"

DATE_FORMATS = {
    "br": "%d/%m/%Y",
}

JINJA_ENVIRONMENT = {"extensions": ["jinja2.ext.i18n"]}

DEFAULT_PAGINATION = 5

DIRECT_TEMPLATES = ["index", "archives", "tags"]

ARTICLE_URL = "post/{slug}.html"
ARTICLE_SAVE_AS = "post/{slug}.html"

AUTHOR_SAVE_AS = ""
CATEGORY_SAVE_AS = ""
